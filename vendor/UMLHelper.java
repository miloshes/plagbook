package a.b.c.umlhelper;

import umlhelper.service.ServiceInterface;
import umlhelper.gui.MainFrameInterface;
import umlhelper.gui.MainFrame;
import umlhelper.service.Service;
import java.io.File;
import a.b.c.d.E;

/**
 * <p>Title: UML Helper</p>
 *
 * <p>Description: This is a test class for parsing, it does not compile.</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * @author John valentino II
 * @version 1.0
 */
public class UMLHelper extends AB implements ServiceInterface, MainFrameInterface, A, B, C {

    /** Represents the gui tier of the application */
    private MainFrame mainFrame;
    /** Represents the service tier of the application */
    private Service service;
    /** Represents the version of this program */
    private float version = 0.1f;
    private ArrayList<String> list;

    /**
     * Entry point for the application
     * @param args String[]
     */
    public static void main(String[] args) {
        new UMLHelper();
    }

    /**
     * Creates the application
     */
    public UMLHelper() {
        this.service = new Service(this);
        this.mainFrame = new MainFrame(this);
    }

    /**
     * Returns the vesion of this program
     * @return float
     */
    public float getVersion() {
        return this.version;
    }

    /**
     * Opens the given directory and searches it for source code files and
     * other directories
     * @param directory File
     */
    public void open(File directory) {
        service.open(directory);
    }


}