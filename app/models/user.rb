# == Schema Information
#
# Table name: users
#
#  id              :integer(4)      not null, primary key
#  first_name      :string(255)
#  last_name       :string(255)
#  login           :string(255)
#  password_digest :string(255)
#  group_id        :integer(4)
#  created_at      :datetime        not null
#  updated_at      :datetime        not null
#  remember_token  :string(255)
#

class User < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :login, :password, :password_confirmation, :group_id
  has_secure_password
  before_save :create_remember_token
  
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :login, presence: true, length: { maximum: 50 }, uniqueness: { case_sensitive: false }
  validates :password, length: { minimum: 6 }
  
  has_many :subjects, foreign_key: "teacher_id"
  has_many :solutions, dependent: :destroy
  has_many :school_years
  has_one :general_profile, dependent: :destroy
  has_one :java_profile, dependent: :destroy
  has_one :c_profile, dependent: :destroy
  belongs_to :group
  
  def full_name
    [first_name, last_name].join(' ')
  end
  
  def sol_gen_profiles
    GeneralProfile.joins('LEFT OUTER JOIN solutions ON solution_id = solutions.id').where("solutions.user_id = #{self.id}")
  end
  
  def sol_java_profiles
    JavaProfile.joins('LEFT OUTER JOIN solutions ON solution_id = solutions.id').where("solutions.user_id = #{self.id}")
  end
  
  def sol_c_profiles
    CProfile.joins('LEFT OUTER JOIN solutions ON solution_id = solutions.id').where("solutions.user_id = #{self.id}")
  end
  
  # get base solutions' profiles
  def base_sol_gen_profiles
    GeneralProfile.joins('LEFT OUTER JOIN solutions ON solution_id = solutions.id').where("solutions.user_id = #{self.id} AND status = \"#{STATUS_BASE}\"")
  end
  
  def base_sol_java_profiles
    JavaProfile.joins('LEFT OUTER JOIN solutions ON solution_id = solutions.id').where("solutions.user_id = #{self.id} AND status = \"#{STATUS_BASE}\"")
  end
  
  def base_sol_c_profiles
    CProfile.joins('LEFT OUTER JOIN solutions ON solution_id = solutions.id').where("solutions.user_id = #{self.id} AND status = \"#{STATUS_BASE}\"")
  end
  
  def build_profiles
    gen_base_solutions = 0
    java_base_solutions = 0
    c_base_solutions = 0
    
    self.solutions.each do |solution|
      
      #when author has no profile yet
      if(gen_base_solutions == 0)
        student_gen_profile = solution.general_profile.dup    #use solution's profile also for author
        student_gen_profile.user_id = self.id
        student_gen_profile.solution_id = nil
        student_gen_profile.save
        
        solution.general_profile.update_attribute("status", STATUS_BASE)
        gen_base_solutions += 1
      else
        if(gen_base_solutions < NUMBER_OF_BASE_SOLUTIONS)
          #this solution will be used as a base for student's profile
          if(PROFILE_ADJUSTMENT_METHOD == "average_2")
            self.general_profile.adjust(solution.general_profile)
          end
          solution.general_profile.update_attribute("status", STATUS_BASE)
         
          gen_base_solutions += 1
        end
      end
      
      case solution.prog_language_id
        when LANG_JAVA
          
          #when author has no profile yet
          if(java_base_solutions == 0)
            student_java_profile = solution.java_profile.dup    #use solution's profile also for author
            student_java_profile.user_id = self.id
            student_java_profile.solution_id = nil
            student_java_profile.save
            
            solution.java_profile.update_attribute("status", STATUS_BASE)
            java_base_solutions += 1
          else                                  
            if(java_base_solutions <= NUMBER_OF_BASE_SOLUTIONS)
              #this solution will be used as a base for student's profile
              if(PROFILE_ADJUSTMENT_METHOD == "average_2")
                self.java_profile.adjust(solution.java_profile)
              end
              solution.java_profile.update_attribute("status", STATUS_BASE)
              
              java_base_solutions += 1
            end
          end
          
        when LANG_C
          #when author has no profile yet
          if(c_base_solutions == 0)
            student_c_profile = solution.c_profile.dup    #use solution's profile also for author
            student_c_profile.user_id = self.id
            student_c_profile.solution_id = nil
            student_c_profile.save
            
            solution.c_profile.update_attribute("status", STATUS_BASE)
           
            c_base_solutions += 1
          else                                
            if(c_base_solutions <= NUMBER_OF_BASE_SOLUTIONS)
              #this solution will be used as a base for student's profile
              if(PROFILE_ADJUSTMENT_METHOD == "average_2")
                self.c_profile.adjust(solution.c_profile)
              end
              solution.c_profile.update_attribute("status", STATUS_BASE)
              c_base_solutions += 1
            end
          end
      end
    end
    
    # adjust user profile using average_all or median
    if(PROFILE_ADJUSTMENT_METHOD != "average_2")
      self.general_profile.adjust(nil)
      if(self.java_profile)
        self.java_profile.adjust(nil)
      end
      if(self.c_profile)
        self.c_profile.adjust(nil)
      end
    end
      
    # create weight profile
    self.solutions.take_while { |s| s.general_profile.status == STATUS_BASE}.each do |solution|
      
    end
   
  end
  
  private

    def create_remember_token
      self.remember_token = SecureRandom.urlsafe_base64
    end
end
