# == Schema Information
#
# Table name: school_years
#
#  id         :integer(4)      not null, primary key
#  year       :integer(4)
#  user_id    :integer(4)
#  created_at :datetime
#  updated_at :datetime
#

class SchoolYear < ActiveRecord::Base
  belongs_to :user
end
