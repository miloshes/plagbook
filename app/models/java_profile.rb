class JavaProfile < ActiveRecord::Base
  
  belongs_to :solution
  belongs_to :user
  
  def self.distance_vector_length(values)
      vector_length = 0.0
      
      values.each do |value|
        vector_length += value
      end
      
      return vector_length
  end
  
  def self.distance_measure(first_id, second_id, save=true)
    distance = 0.0
    vector_length_1 = 0.0
    vector_length_2 = 0.0
    
    first_profile = JavaProfile.find(first_id, select: "javadoc_ratio, try_catch_ratio, throws_ratio", limit: 1)
    second_profile = JavaProfile.find(second_id, select: "javadoc_ratio, try_catch_ratio, throws_ratio", limit: 1)
    
    first_profile.attributes.each do |metric|
      vector_length_1 += metric[1]
    end
    
    second_profile.attributes.each do |metric|
      vector_length_2 += metric[1]
    end
    
    # if(vector_length_1 > 0 && vector_length_2 > 0)
      # for i in(0..first_profile.attributes.values.size-1)
        # distance += ((first_profile.attributes.values[i]/vector_length_1) - (second_profile.attributes.values[i]/vector_length_2)).abs
      # end
    # else
      # distance = (vector_length_1 - vector_length_2).abs  #TODO verify
    # end
    
    #without normalization
    for i in(0..first_profile.attributes.values.size-1)
      distance += (first_profile.attributes.values[i] - second_profile.attributes.values[i]).abs
    end
    
    if(save)
      Comparison.create(profile_type: PROFILE_JAVA, solution_profile_id: first_id, student_profile_id: second_id, distance: distance)
      
      return distance
    else
      comp = Comparison.new(profile_type: PROFILE_JAVA, solution_profile_id: first_id, student_profile_id: second_id, distance: distance)
      
      return comp
    end
    
  end
  
  def self.euclidian_vector_length(values)
      vector_length = 0.0
      
      values.each do |value|
        vector_length += value**2
      end
      
      vector_length = Math.sqrt(vector_length)
      
      return vector_length
  end
  
  def self.euclidian_distance(first_id, second_id, save=true)
    euclidian = 0.0
    vector_length_1 = 0.0
    vector_length_2 = 0.0
    
    first_profile = JavaProfile.find(first_id, select: "javadoc_ratio, try_catch_ratio, throws_ratio", limit: 1)
    second_profile = JavaProfile.find(second_id, select: "javadoc_ratio, try_catch_ratio, throws_ratio", limit: 1)
    
    first_profile.attributes.each do |metric|
      vector_length_1 += metric[1]**2 #power of 2
    end
    
    second_profile.attributes.each do |metric|
      vector_length_2 += metric[1]**2
    end
    
    vector_length_1 = Math.sqrt(vector_length_1)
    vector_length_2 = Math.sqrt(vector_length_2)
    
    if(vector_length_1 > 0 && vector_length_2 > 0)
      for i in(0..first_profile.attributes.values.size-1)
        euclidian += ((first_profile.attributes.values[i]/vector_length_1) - (second_profile.attributes.values[i]/vector_length_2))**2
      end
    else
      euclidian = (vector_length_1 - vector_length_2)**2  #TODO verify
    end
    
    euclidian = Math.sqrt(euclidian)
    
    if(save)
      Comparison.create(profile_type: PROFILE_JAVA, solution_profile_id: first_id, student_profile_id: second_id, distance: euclidian)
      return euclidian
    else
      comp = Comparison.new(profile_type: PROFILE_JAVA, solution_profile_id: first_id, student_profile_id: second_id, distance: euclidian)
      return comp
    end
  end
  
  def self.cosine_distance(first_id, second_id, save=true)
    cosine = 0.0
    vector_length_1 = 0.0
    vector_length_2 = 0.0
    
    first_profile = JavaProfile.find(first_id, select: "javadoc_ratio, try_catch_ratio, throws_ratio", limit: 1)
    second_profile = JavaProfile.find(second_id, select: "javadoc_ratio, try_catch_ratio, throws_ratio", limit: 1)
    
    first_profile.attributes.each do |metric|
      vector_length_1 += metric[1]**2 #power of 2
    end
    
    second_profile.attributes.each do |metric|
      vector_length_2 += metric[1]**2
    end
    
    vector_length_1 = Math.sqrt(vector_length_1)
    vector_length_2 = Math.sqrt(vector_length_2)
    
    for i in(0..first_profile.attributes.values.size-1)
      cosine += (first_profile.attributes.values[i]) * (second_profile.attributes.values[i])
    end
    
    if(cosine > 0)
      cosine = cosine / (vector_length_1 * vector_length_2)
    else
      cosine = 0
    end
    
    if(save)
      Comparison.create(profile_type: PROFILE_JAVA, solution_profile_id: first_id, student_profile_id: second_id, distance: cosine)
      return cosine
    else
      comp = Comparison.new(profile_type: PROFILE_JAVA, solution_profile_id: first_id, student_profile_id: second_id, distance: cosine)
      return comp
    end
  end
  
  # use values from base solutions to determine attribute weights specific to the user
  def compute_weights
    attributes_count = self.attributes.values.size - 3 #without created/updated_at
    
    all_base_profiles = self.user.base_sol_java_profiles
      
    variances_sum = 0
    
    weight_profile = JavaProfile.new
    weight_profile.user_id = self.user.id
    weight_profile.status = STATUS_WEIGHT 
    
    for i in(4..attributes_count) #leaving out id, user_id, solution _id and status
      attribute_variance = 0
      base_values_array = []
      
      all_base_profiles.each do |base_profile|
        base_values_array.push(base_profile.attributes.values[i].to_f)
      end
      
      base_values_array.combination(2).to_a.each do |combination|
        attribute_variance += (combination[0] - combination[1]).abs
      end
      
      weight_profile.assign_attributes(self.attributes.keys[i] => attribute_variance)
      variances_sum += attribute_variance
    end
    
   for i in (4..attributes_count)
      weight_profile.assign_attributes(weight_profile.attributes.keys[i] => 1-(weight_profile.attributes.values[i]/variances_sum))
   end
    
    weight_profile.save
  end
  
  def adjust(solution_profile)
    attributes_count = self.attributes.values.size - 3 #without created/updated_at
    
    if(PROFILE_ADJUSTMENT_METHOD == "average_2")
      for i in(4..attributes_count) #leaving out id, user_id, solution _id and status
        if(i == 5 && solution_profile.attributes.values[i] == 0 && solution_profile.attributes.values[i+1] == 0)
          break
        end       #when ratio values are 0 don't adjust with them
        
        sum = self.attributes.values[i] + solution_profile.attributes.values[i] 
        diff = (self.attributes.values[i]/sum - solution_profile.attributes.values[i]/sum).abs
        
        if(diff <= ADJUSTMENT_TRESHOLD)
          self.update_attribute(self.attributes.keys[i], sum/2)
        end
      end
    end
    
    if(PROFILE_ADJUSTMENT_METHOD == "average_all")
      all_sol_profiles = []
      
      all_sol_profiles = self.user.base_sol_java_profiles
      #all_sol_profiles = self.user.sol_java_profiles
      
      for i in(4..attributes_count) #leaving out id, user_id, solution _id and status
        sum = 0
        count = all_sol_profiles.length
        
        all_sol_profiles.each do |sol_profile|
          if(i == 5 && sol_profile.attributes.values[i] == 0 && sol_profile.attributes.values[i+1] == 0)
            count -= 1
            next
          end       #when ratio values are 0 don't adjust with them
          sum += sol_profile.attributes.values[i]
        end
        if(count > 0)
          self.update_attribute(self.attributes.keys[i], sum/count)
        end
      end
    end
    
    if(PROFILE_ADJUSTMENT_METHOD == "median")
      all_sol_profiles = []
      
      #all_sol_profiles = self.user.base_sol_java_profiles
      all_sol_profiles = self.user.sol_java_profiles
      
      for i in(4..attributes_count) #leaving out id, user_id, solution _id and status
        sum = 0
        count = all_sol_profiles.length
        values_array = []
        
        all_sol_profiles.each do |sol_profile|
          values_array.push(sol_profile.attributes.values[i].to_f)
        end
        
        length = values_array.length
        sorted = values_array.sort
        median = (length % 2 == 1)? sorted[length/2] : (sorted[length/2-1] + sorted[length/2])/2
        
        self.update_attribute(self.attributes.keys[i], median)
      end
    end     
  end
  
  #compares java profile (of solution) with java profiles of users or their solutions
  #solving the same assignment 
  def compare_with_others(authors_distance)
    PLAGBOOK_LOG.info("Comparing java profile ID " + self.id.to_s)
    
    self_solution = self.solution
    
    if(COMPARE_PROFILES)  #comparing profiles of other students solving the same assignment
      same_assignment_solutions = Solution.find(:all, conditions: "id != #{self_solution.id} AND assignment_id = #{self_solution.assignment_id}",
                                          select: "user_id", group: "user_id")  # initializing a model object with only selected fields
                                                            #group acts like distinct
      ids = ""
      same_assignment_solutions.each do |solution|
        ids += solution.user_id.to_s + ","  #using user IDs
      end
      
      if(ids.length > 0)
        ids = ids[0,ids.size-1] #removing last ,
        java_profiles = JavaProfile.find(:all, conditions: "user_id IN (#{ids})")
      end
      
    else  #comparing profiles of solutions of the same assignment
      
      same_assignment_solutions = Solution.find(:all, conditions: "id != #{self_solution.id} AND assignment_id = #{self_solution.assignment_id}",
                                          select: "id", group: "id")  # initializing a model object with only selected fields
                                                       #group acts like distinct
      ids = ""
      same_assignment_solutions.each do |solution|
        ids += solution.id.to_s + "," #using solution IDs
      end
      
      if(ids.length > 0)
        ids = ids[0,ids.size-1] #removing last ,
        java_profiles = JavaProfile.find(:all, conditions: "solution_id IN (#{ids})")
      end
    end
    
    #COMMON PART
    
    if(java_profiles)  #there are some profiles to compare
      comparison_results = []
        
      java_profiles.each do |profile|
        case COMPARISON_METHOD
          when "distance_measure"
            comparison_results.push(JavaProfile.distance_measure(self.id, profile.id, false))
          when "euclidian_distance"
            comparison_results.push(JavaProfile.euclidian_distance(self.id, profile.id, false))
          when "cosine_distance"
            comparison_results.push(JavaProfile.cosine_distance(self.id, profile.id, false))
        end
      end
  
      if(COMPARISON_METHOD == "cosine_distance")
        comparison_results.sort! { |a,b| b.distance <=> a.distance } #DESC because 1 is value for similarity here
      else
        comparison_results.sort! { |a,b| a.distance <=> b.distance } #ASC because here it's 0
      end
      
      #save only those comparisons which are more similar than solution's author
      comparison_results.each do |comparison|
        save = false
        
        if(COMPARISON_METHOD == "cosine_distance")
          if(comparison.distance > authors_distance)
            save = true
          end
        else
          if(comparison.distance <= authors_distance)
            save = true
          end
        end
        
        if(save)
          if(COMPARE_PROFILES)
            comparison.profile_type = PROFILE_JAVA
          else
            comparison.profile_type = PROFILE_JAVA + "-solutions" #means that second profile_id also belongs to solution
          end
          
          self.update_attribute("status", STATUS_SUSPICIOUS)
          #self_solution.update_attribute("suspicious", true)
          
          comparison.save
        else
          break
        end
      end
    end
  end
  
end
