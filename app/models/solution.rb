# == Schema Information
#
# Table name: solutions
#
#  id            :integer(4)      not null, primary key
#  filename      :text
#  assignment_id :integer(4)
#  user_id       :integer(4)
#  created_at    :datetime        not null
#  updated_at    :datetime        not null
#  source_code   :text(2147483647
#

class Solution < ActiveRecord::Base
  require 'wtf_lang'
  
  WtfLang::API.key = "38ff07e15f48684a09c2977d9ff82946"
  
  attr_accessible :filename, :source_code, :assignment_id, :prog_language_id
  
  validates :user_id, presence: true
  validates :assignment_id, presence: true
  validates :filename, presence: true
  
  belongs_to :assignment
  belongs_to :user
  belongs_to :prog_language
  
  has_one :general_profile, dependent: :destroy
  has_one :java_profile, dependent: :destroy
  has_one :c_profile, dependent: :destroy
  
  #default_scope order: 'solutions.created_at DESC'
  
  def build_profiles
    self.compute_general_metrics
    
    case self.prog_language_id
      when LANG_JAVA
        self.compute_java_metrics
      when LANG_C
        self.compute_c_metrics
    end
  end
  
  def is_suspicious?
    if(JOIN_PROFILES == true)
      if(COMPARISON_METHOD == "cosine_distance")
        comparisons = Comparison.find(:all, conditions: "solution_profile_id = #{self.general_profile.id} AND profile_type = " + "\"" + PROFILE_JOINED + self.prog_language.language + "\"", order: 'distance DESC')
      else
        comparisons = Comparison.find(:all, conditions: "solution_profile_id = #{self.general_profile.id} AND profile_type = " + "\"" + PROFILE_JOINED + self.prog_language.language + "\"", order: 'distance ASC')
      end
      
      if(comparisons.count > 0 && comparisons.first.student_profile_id != self.user.general_profile.id)
        i=1
        comparisons.each do |comparison|
          if(comparison.student_profile_id == self.user.general_profile.id)
            self.update_attribute(:suspicious, i)
          else
            i += 1
          end
        end
        return true
      else
        self.update_attribute(:suspicious, 1)
        return false
      end
    else 
      #TODO
    end
  end
  
  def compare_profiles
    
    same_assignment_solutions = Solution.find(:all, conditions: "id != #{self.id} AND assignment_id = #{self.assignment_id}",
                                          select: "user_id", group: "user_id")  # initializing a model object with only selected fields
                                                            #group acts like distinct
    ids = ""
    same_assignment_solutions.each do |solution|
      ids += solution.user_id.to_s + ","  #using user IDs
    end
    
    if(ids.length > 0)
      ids = ids[0,ids.size-1] #removing last ,
      gen_profiles = GeneralProfile.find(:all, conditions: "user_id IN (#{ids}) AND status IS NULL")
    end
    
    if(gen_profiles != nil && JOIN_PROFILES == true)
      gen_profiles.each do |gen_profile|
       case self.prog_language_id
        when LANG_JAVA
          case COMPARISON_METHOD
            when "distance_measure"
              gen_profile.distance_measure_java(self.general_profile.id, self.java_profile.id, gen_profile.id, JavaProfile.where(:user_id => gen_profile.user_id, :status => nil), save=true)
            when "euclidian_distance"
              gen_profile.euclidian_distance_java(self.general_profile.id, self.java_profile.id, gen_profile.id, JavaProfile.where(:user_id => gen_profile.user_id, :status => nil), save=true)
            when "cosine_distance"
              gen_profile.cosine_distance_java(self.general_profile.id, self.java_profile.id, gen_profile.id, JavaProfile.where(:user_id => gen_profile.user_id, :status => nil), save=true)
          end
        when LANG_C
          case COMPARISON_METHOD
            when "distance_measure"
              gen_profile.distance_measure_c(self.general_profile.id, self.c_profile.id, gen_profile.id, CProfile.where(:user_id => gen_profile.user_id, :status => nil), save=true)
            when "euclidian_distance"
              gen_profile.euclidian_distance_c(self.general_profile.id, self.c_profile.id, gen_profile.id, CProfile.where(:user_id => gen_profile.user_id, :status => nil), save=true)
            when "cosine_distance"
              gen_profile.cosine_distance_c(self.general_profile.id, self.c_profile.id, gen_profile.id, CProfile.where(:user_id => gen_profile.user_id, :status => nil), save=true)
          end
       end
      end
    else
      #TODO
      # compare only general profiles - what difference do lang profiles make?
    end
    
  end
  
  #generates general and language specific profiles for solution
  #returns array with info about suspicious profiles
  def create_profiles
      gen_distance = 0.0
      java_distance = 0.0
      c_distance = 0.0
      
      gen_base = false
      lang_base = false
      
      #GENERAL PROFILE
      sol_gen_profile = self.compute_general_metrics
      
      user = self.user
      
      #when author has no profile yet
      if(!user.general_profile)
        student_gen_profile = sol_gen_profile.dup    #use solution's profile also for author
        student_gen_profile.user_id = user.id
        student_gen_profile.solution_id = nil
        student_gen_profile.save
        
        sol_gen_profile.update_attribute("status", STATUS_BASE)
        gen_base = true
      else
        if(user.sol_gen_profiles.count <= NUMBER_OF_BASE_SOLUTIONS)
          #this solution will be used as a base for student's profile
          user.general_profile.adjust(sol_gen_profile)
          sol_gen_profile.update_attribute("status", STATUS_BASE)
          gen_base = true
        else
          case COMPARISON_METHOD
            when "distance_measure"
              gen_distance = GeneralProfile.distance_measure(self.general_profile.id, user.general_profile.id)
            when "euclidian_distance"
              gen_distance = GeneralProfile.euclidian_distance(self.general_profile.id, user.general_profile.id)
            when "cosine_distance"
              gen_distance = GeneralProfile.cosine_distance(self.general_profile.id, user.general_profile.id)
          end
        end
      end
     
      case self.prog_language_id
        when LANG_JAVA
          
          #CREATE JAVA PROFILE
          
          sol_java_profile = self.compute_java_metrics
          
          #when author has no profile yet
          if(!user.java_profile)
            student_java_profile = sol_java_profile.dup    #use solution's profile also for author
            student_java_profile.user_id = user.id
            student_java_profile.solution_id = nil
            student_java_profile.save
            
            sol_java_profile.update_attribute("status", STATUS_BASE)
            lang_base = true
          else                                  
            if(user.sol_java_profiles.count <= NUMBER_OF_BASE_SOLUTIONS)
              #this solution will be used as a base for student's profile
              user.java_profile.adjust(sol_java_profile)
              sol_java_profile.update_attribute("status", STATUS_BASE)
              lang_base = true 
            else
              case COMPARISON_METHOD
                when "distance_measure"
                  java_distance = JavaProfile.distance_measure(sol_java_profile.id, user.java_profile.id)
                when "euclidian_distance"
                  java_distance = JavaProfile.euclidian_distance(sol_java_profile.id, user.java_profile.id)
                when "cosine_distance"
                  java_distance = JavaProfile.cosine_distance(sol_java_profile.id, user.java_profile.id)
              end
            end
          end
          
        when LANG_C
          
          #CREATE C PROFILE
          
          sol_c_profile = self.compute_c_metrics
          
          #when author has no profile yet
          if(!user.c_profile)
            student_c_profile = sol_c_profile.dup    #use solution's profile also for author
            student_c_profile.user_id = user.id
            student_c_profile.solution_id = nil
            student_c_profile.save
            
            sol_c_profile.update_attribute("status", STATUS_BASE)
            lang_base = true
          else                                
            if(user.sol_c_profiles.count <= NUMBER_OF_BASE_SOLUTIONS)
              #this solution will be used as a base for student's profile
              user.c_profile.adjust(sol_c_profile)
              sol_c_profile.update_attribute("status", STATUS_BASE)
              lang_base = true
            else
              case COMPARISON_METHOD
                when "distance_measure"
                  c_distance = CProfile.distance_measure(sol_c_profile.id, user.c_profile.id)
                when "euclidian_distance"
                  c_distance = CProfile.euclidian_distance(sol_c_profile.id, user.c_profile.id)
                when "cosine_distance"
                  c_distance = CProfile.cosine_distance(sol_c_profile.id, user.c_profile.id)
              end
            end
          end
      end
      
      result = []
      suspicisous = false    
      
      if(!gen_base)
      #if(gen_distance >= SUSPICIOUS_TRESHOLD)
        #suspicious = true
        
        #sol_gen_profile.update_attribute("status", STATUS_SUSPICIOUS)
        result.push [sol_gen_profile, gen_distance]
      end
      
      if(!lang_base)
      #if(java_distance >= SUSPICIOUS_TRESHOLD)
        #suspicious = true
        case self.prog_language_id
          when LANG_JAVA
            result.push [sol_java_profile, java_distance]
          when LANG_C
            result.push [sol_c_profile, c_distance]
        end
      end
       
      # if(c_distance >= SUSPICIOUS_TRESHOLD)
        # suspicious = true
#         
        # sol_c_profile.update_attribute("status", STATUS_SUSPICIOUS)
        # result.push [sol_c_profile, c_distance]
      # end
      
      # if(suspicious)
        # self.update_attribute("suspicious", true)
      # end
      
      #return array of suspicious profiles
      return result
  end
  
  #computes general metrics
  def compute_general_metrics
    general_profile = GeneralProfile.new(solution_id: self.id)
    
    comment_metrics = get_comment_metrics
    
    general_profile.comm_code_ratio = comment_metrics[:comm_code_ratio]
    general_profile.single_comm_ratio = comment_metrics[:single_comm_ratio]
    general_profile.multi_comm_ratio = comment_metrics[:multi_comment_ratio]
    general_profile.avg_comm_length = comment_metrics[:avg_comm_length]
    general_profile.comm_english_ratio = comment_metrics[:comm_english_ratio]
    
    code_metrics = get_code_metrics
    
    general_profile.avg_line_length = code_metrics[:avg_line_length]
    general_profile.ws_distribution = code_metrics[:whitespace_distrib]
    general_profile.words_per_line  = code_metrics[:words_line_ratio]
    general_profile.avg_fun_length  = code_metrics[:avg_function_length]
    general_profile.var_english_ratio  = code_metrics[:var_english_ratio]
    general_profile.avg_var_length  = code_metrics[:avg_var_length]
    general_profile.avg_fun_name_length  = code_metrics[:avg_fun_name_length]
    
    
    structure_metrics = get_structure_metrics
    
    general_profile.for_loop_ratio = structure_metrics[:for_loop_ratio]
    general_profile.while_loop_ratio = structure_metrics[:while_loop_ratio]
    general_profile.do_loop_ratio = structure_metrics[:do_loop_ratio]
    
    general_profile.save
    
    return general_profile
  end
  
  #computes java metrics
  def compute_java_metrics
    java_profile = JavaProfile.new(solution_id: self.id)
    comment_metrics = get_java_metrics
    
    java_profile.javadoc_ratio = comment_metrics[:javadoc_ratio]
    java_profile.try_catch_ratio = comment_metrics[:try_catch_ratio]
    java_profile.throws_ratio = comment_metrics[:throws_ratio]
    
    java_profile.save
        
    return java_profile
  end
  
  #computes c metrics
  def compute_c_metrics
    c_profile = CProfile.new(solution_id: self.id)
    operator_metrics = get_c_metrics

    c_profile.unary_ratio = operator_metrics[:unary_ratio]
    c_profile.short_operator_ratio = operator_metrics[:short_operator_ratio]
    
    c_profile.save
    
    return c_profile
  end
  
  private
    
    def get_code_metrics
      in_function = false
      lines_count = 0
      chars_count = 0
      words_count = 0
    
      white_lines_count = 0
      whitespaces_count = 0
      is_whitespace_line = false
    
      functions_count = 0
      fun_lines_count = 0
      left_brackets = 0
      
      variables = ""
      var_english_ratio = 0
      fun_names = ""
      var_char_count = 0
      avg_var_length = 0
      fun_char_count = 0
      avg_fun_name_length = 0
      
      begin
        #TODO no connection
        if (self.variables + " " + self.fun_names).full_lang == "english"
          var_english_ratio = 1
        end  
      rescue
        puts "DETECT LANGUAGE ERROR"
        comm_english_ratio = 0
      end
      
      if(self.variables.length > 0)
        self.variables.split(" ").each do |var|
           var_char_count += var.length
        end
        avg_var_length = var_char_count.to_f / self.variables.split(" ").size.to_f
      end
      
      if(self.fun_names.length > 0)
        self.fun_names.split(" ").each do |fun|
           fun_char_count += fun.length
        end
        avg_fun_name_length = fun_char_count.to_f / self.fun_names.split(" ").size.to_f
      end
      
      function_regex = '.*\w+ (\w)+\s*\([^\)]*\)\s*[^;]*'
      
      self.source_code.each_line do |line|
        if(line.strip.length > 1)
            chars_count += line.length
            lines_count += 1
        end
          
        # white space distrib
        i = 0
        while(!("" == line) && (line[i..i] == " ") || (line[i..i] == "\t")) do
          is_whitespace_line = true
          whitespaces_count += 1
          if((i+1) < line.length)
            i += 1
          else
            break
          end
        end
        
        if(is_whitespace_line)
          white_lines_count += 1
          is_whitespace_line = false
        end
          
        #avg function length
        line = line.strip()
          
        if(in_function)
          if(line.include?("{"))
            left_brackets += 1
          end
            
          if(left_brackets > 0)
            if(line.include?("}"))
              left_brackets -= 1
            end
              
            if(line.length > 0 && left_brackets > 0) #ignore empty lines
              fun_lines_count += 1
            end
              
            if(left_brackets ==0)
              in_function = false;
            end
          end
        end
        
        if((line.length > 5) && (line.match(function_regex))) #to eliminate some comparisons
          if(!line.include?("else ")) #TODO eliminate this go around
            functions_count += 1
            in_function = true
              
            if(line.include?("{"))
              left_brackets += 1
            end
          end
        end
      end
      
      words_count = self.source_code.split.size
      
      if(lines_count > 0)
        avg_line_length = chars_count.to_f/lines_count
      else
        avg_line_length = 0;
      end
      
      if(white_lines_count > 0)
        whitespace_distrib = whitespaces_count.to_f/white_lines_count
      else
        whitespace_distrib = 0
      end
      
      if(functions_count > 0)
        avg_function_length = fun_lines_count.to_f/functions_count
      else
        avg_function_length = 0
      end
      
      if(lines_count > 0)
        words_line_ratio = words_count.to_f/lines_count
      else
        words_line_ratio = 0
      end
      
      return { avg_line_length: avg_line_length, whitespace_distrib: whitespace_distrib, 
               avg_function_length: avg_function_length, words_line_ratio: words_line_ratio,
               var_english_ratio: var_english_ratio, avg_var_length: avg_var_length, avg_fun_name_length: avg_fun_name_length }
    end
    
    def get_comment_metrics
      comments_count = 0
      code_count = 0
      single_comments_count = 0
      multi_comments_count = 0
      comm_english_ratio = 0
      comments = ""
      code = ""
      commented_code = ""

      comment_regex = '(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|(//.*)' #TODO verify
      #source http://ostermiller.org/findcomment.html
      
      is_multiline = false
      
      self.source_code.each_line do |line|
        line = line.strip
        
        if((line.length == 2) && (line.include?("/*")))
            is_multiline = true
            next
        end
        
        if(is_multiline && line.length > 0)
          if(line.include?("*/"))
             is_multiline = false
             if(line.length > 2)
               if(line[-1,1] != ";" && line[-1,1] != "{" && line[-1,1] != "}")   # don't include commented code in comments 
                 comments += line.delete("*/").strip + "\n"
                 comments_count += 1
               else
                 commented_code += line.delete("*/").strip + "\n"
               end
             end
             next
          else
             if(line[-1,1] != ";" && line[-1,1] != "{" && line[-1,1] != "}")
               comments += line.strip + "\n"
               comments_count += 1
             else
               commented_code += line.strip + "\n"
             end
             next
          end
        end
     
        if(line.match(comment_regex))
          if(line.include?("//"))
            if(line[-1,1] != ";" && line[-1,1] != "{" && line[-1,1] != "}")
              comments += line[line.index("//")+2..line.length].strip + "\n"
              single_comments_count += 1
            else
              commented_code += line[line.index("//")+2..line.length].strip + "\n"
            end
            
            # check if there is code in the same line
            if(line.index("//") > 0)
              code += line[0..line.index("//")-1].strip + "\n"
              code_count += 1
            end
          else
            if((line.include?("/*")) && (line.length > 2))
              if(line[-1,1] != ";" && line[-1,1] != "{" && line[-1,1] != "}")
                comments += line[line.index("/*")+2..((line.include?("*/"))? line.index("*/")-1:line.length)].strip + "\n"
                multi_comments_count += 1
              else
               commented_code += line[line.index("/*")+2..((line.include?("*/"))? line.index("*/")-1:line.length)].strip + "\n"
              end
              
              if(line.index("/*") > 0)
                code += line[0..line.index("/*")-1].strip + "\n"
                code_count += 1
              end
            else
              if((line.include?("*/")) && (line.length > 2))
                if(line[-1,1] != ";" && line[-1,1] != "{" && line[-1,1] != "}")
                  comments += line.delete("*/").strip + "\n"
                  multi_comments_count += 1
                else
                  commented_code += line.delete("*/").strip + "\n"
                end
              else
                if((line.match('\**\s*\w+.*')) && (line.length > 1))
                  if(line[-1,1] != ";" && line[-1,1] != "{" && line[-1,1] != "}")
                    comments += line[0 .. line.index('*')+1].strip + "\n"
                    multi_comments_count += 1
                  else
                    commented_code += line[0 .. line.index('*')+1].strip + "\n"
                  end
                end
              end
            end
          end
          comments_count += 1
        else
          if(line.length > 0) #ignore empty lines
            code += line + "\n"
            code_count += 1
          end
        end
      end
      
      self.update_attribute(:code, code)
      self.update_attribute(:comments, comments)
      self.update_attribute(:commented_code, commented_code)
      
      begin
        #TODO no connection
        if comments.full_lang == "english"
          comm_english_ratio = 1  
        end
      rescue
        puts "DETECT LANGUAGE ERROR"
        comm_english_ratio = 0
      end
       
      
      if(comments_count > 0)
        single_comm_ratio = single_comments_count.to_f / comments_count
        multi_comm_ratio = multi_comments_count.to_f / comments_count
        avg_comm_length = comments.length.to_f / comments_count 
      else
        single_comm_ratio = 0
        multi_comm_ratio = 0
        avg_comm_length = 0
      end
    
      comm_code_ratio = comments_count.to_f / code_count
      
      
      return { comm_code_ratio: comm_code_ratio, single_comm_ratio: single_comm_ratio,
              multi_comment_ratio: multi_comm_ratio, avg_comm_length: avg_comm_length, comm_english_ratio: comm_english_ratio }
    end
    
    def get_structure_metrics
      for_count = 0
      while_count = 0
      do_count = 0
      loop_count = 0
      
      #TODO ignore commented out loops 
      #forRegex1 = "([^//]|[^/\*]).*\s*for\s*\(.*"
      
      forRegex1 = '.*\s*for\s*\(.*'
      forRegex2 = ',*\s*for\s*'
    
      whileRegex1 = '.*\s*while\s*\(.*'
      whileRegex2 = '.*\s*while\s*'
    
      doRegex1 = '.*\s*do\s*\{.*'
      doRegex2 = '.*\s*do\s*'
      
      self.source_code.each_line do |line|
        line = line.strip
          
        if(line.length < 3)
          next
        end
          
        #TODO considering only one loop in a line
        #if((line.match(forRegex1)) || (line.match(forRegex2)))
        if(line.match(forRegex1))
            for_count += 1
            next
        end
          
        if((line.match(whileRegex1)) || (line.match(whileRegex2)))
            while_count += 1
            next
        end
          
        #if((line.match(doRegex1)) || (line.match(doRegex2)))
        if(line.match(doRegex1))
            do_count += 1
        end
      end

      if(do_count > 0)
          while_count -= do_count; #because of do.. while construct
      end
        
      loop_count = for_count + while_count + do_count;
      
      if(loop_count > 0)
        forLoopRatio = for_count.to_f / loop_count
        whileLoopRatio = while_count.to_f / loop_count
        doLoopRatio = do_count.to_f / loop_count
      else
        forLoopRatio = 0
        whileLoopRatio = 0
        doLoopRatio = 0
      end
      
      return { for_loop_ratio: forLoopRatio, while_loop_ratio: whileLoopRatio, do_loop_ratio: doLoopRatio }
    end
    
    def get_java_metrics
      #number of javadoc lines
      javadoc_count = 0
      comments_count = 0
      try_catch_count = 0
      throws_count = 0
      javadoc_text = ""
      
      javadoc_regex = Regexp.new('[^/](/\*\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*?/)')
      
      javadocs = self.source_code.scan(javadoc_regex)
      
      javadocs.each do |block|
        block = block[0]
          
        block.lines.each do |line|
          line = line.strip

          if((line.include?("/**")) && (line.length > 3))
            javadoc_count += 1
            javadoc_text += line.delete("/**").strip + "\n"
          else
            if((line.include?("*/")) && (line.length > 2))
              javadoc_count += 1
              javadoc_text += line.delete("*/").strip + "\n"
            else
              if((line.match('\**\s*\w+.*')) && (line.length > 1))
                javadoc_count += 1
                javadoc_text += line.delete("*").strip + "\n"
              end
            end
          end
        end
      end
      
      self.update_attribute(:comments, self.comments + javadoc_text) 
      
      comment_regex = Regexp.new('(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|(//.*)')
      
      comments = self.source_code.scan(comment_regex)
      
      comments.each do |block|
        
        if(block[0])  #1st match is javadoc and last is regular comment
          block = block[0]
        else
          block = block[4]
        end
          
        block.lines.each do |line|
          line = line.strip
          
          if(line.length > 3)
            comments_count += 1
          end
        end
      end
      
      if(comments_count > 0)
        if(comments_count < javadoc_count)
          debugger
          puts "ach"
        end
        javadoc_ratio = javadoc_count.to_f / comments_count
      else
        javadoc_ratio = 0
      end
      
      try_regex = Regexp.new('try(\s*){')
      throws_regex = Regexp.new('\)\s*throws\s*[A-Z]+')
      
      try_blocks_count = self.source_code.scan(try_regex).size
      throws_count = self.source_code.scan(throws_regex).size
      
      exceptions_count = try_blocks_count + throws_count
      
      if(exceptions_count > 0)
        try_catch_ratio = try_blocks_count.to_f / exceptions_count
        throws_ratio = throws_count.to_f / exceptions_count
      else
        try_catch_ratio = 0
        throws_ratio = 0
      end
      
      return { javadoc_ratio: javadoc_ratio, try_catch_ratio: try_catch_ratio, throws_ratio: throws_ratio }
    end
    
    def get_c_metrics
      unary_count = 0
      short_operator_count = 0
      operators_count = 0
      
      unary_regex = Regexp.new('(\+\+|--)\s*[^)]')  #ignoring ;i++) in for loops
      short_operator_regex = Regexp.new('(\+|-)=\s*1')
      
      unary_count = self.source_code.scan(unary_regex).size
      short_operator_count = self.source_code.scan(short_operator_regex).size
      
      operators_count = unary_count + short_operator_count
      
      if(operators_count > 0)
        unary_ratio = unary_count.to_f / operators_count
        short_operator_ratio = short_operator_count.to_f / operators_count
      else
        unary_ratio = 0
        short_operator_ratio = 0
      end
      
      return { unary_ratio: unary_ratio, short_operator_ratio: short_operator_ratio }
    end
  
end