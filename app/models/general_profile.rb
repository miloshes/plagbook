# == Schema Information
#
# Table name: general_profiles
#
#  id                :integer(4)      not null, primary key
#  user_id           :integer(4)
#  avg_line_length   :float
#  ws_distribution   :float
#  words_per_line    :float
#  avg_fun_length    :float
#  avg_comm_length   :float
#  comm_code_ratio   :float
#  comm_language     :string
#  single_comm_ratio :float
#  multi_comm_ratio  :float
#  for_loop_ratio    :float
#  while_loop_ratio  :float
#  do_loop_ratio     :float

#  created_at        :datetime        not null
#  updated_at        :datetime        not null
#

class GeneralProfile < ActiveRecord::Base
  
  belongs_to :solution
  belongs_to :user
  
  def self.distance_vector_length(values)
      vector_length = 0.0
      
      values.each do |value|
        vector_length += value
      end
      
      return vector_length
  end
  
  def distance_measure_java(first_gen_id, first_java_id, second_gen_id, second_java_id, save=true)
    distance = 0.0
    vector_length_1 = 0.0
    vector_length_2 = 0.0
    
    first_gen_profile = GeneralProfile.find(first_gen_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio", limit: 1)
                                          
    first_java_profile = JavaProfile.find(first_java_id, select: "javadoc_ratio, try_catch_ratio, throws_ratio", limit: 1)
                                          
    second_gen_profile = GeneralProfile.find(second_gen_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio", limit: 1)
    
    second_java_profile = JavaProfile.find(second_java_id, select: "javadoc_ratio, try_catch_ratio, throws_ratio", limit: 1)
    
    gen_weight_profile = nil
    java_weight_profile = nil
    
    if(WEIGHTENING == true)
      user_gen_profile = GeneralProfile.find(second_gen_id)
      
      gen_weight_profile = GeneralProfile.find(:all, conditions: "user_id = #{user_gen_profile.user_id} AND status = \"weight\"", select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length").first
                                          
      java_weight_profile = JavaProfile.find(:all, conditions: "user_id = #{user_gen_profile.user_id} AND status = \"weight\"", select: "javadoc_ratio, try_catch_ratio, throws_ratio").first  
    end                                 
    
    first_gen_profile.attributes.each do |metric|
      vector_length_1 += metric[1]
    end
    
    first_java_profile.attributes.each do |metric|
      vector_length_1 += metric[1]
    end
    
    second_gen_profile.attributes.each do |metric|
      vector_length_2 += metric[1]
    end
    
    second_java_profile.attributes.each do |metric|
      vector_length_2 += metric[1]
    end
    
    # for i in(0..first_profile.attributes.values.size-1)
      # distance += ((first_gen_profile.attributes.values[i]/vector_length_1) - (second_gen_profile.attributes.values[i]/vector_length_2)).abs
    # end
    
    # for i in(0..first_profile.attributes.values.size-1)
      # distance += ((first_java_profile.attributes.values[i]/vector_length_1) - (second_java_profile.attributes.values[i]/vector_length_2)).abs
    # end
    
    #without normalization
    
    if(gen_weight_profile != nil && java_weight_profile != nil)
       for i in(0..first_gen_profile.attributes.values.size-1)
        distance += (first_gen_profile.attributes.values[i] - second_gen_profile.attributes.values[i]).abs * gen_weight_profile.attributes.values[i]
      end
       
      for i in(0..first_java_profile.attributes.values.size-1)
        distance += (first_java_profile.attributes.values[i] - second_java_profile.attributes.values[i]).abs * java_weight_profile.attributes.values[i]
      end
    else
      for i in(0..first_gen_profile.attributes.values.size-1)
        distance += (first_gen_profile.attributes.values[i] - second_gen_profile.attributes.values[i]).abs
      end
       
      for i in(0..first_java_profile.attributes.values.size-1)
        distance += (first_java_profile.attributes.values[i] - second_java_profile.attributes.values[i]).abs
      end
    end
        
    if(save)
      Comparison.create(profile_type: PROFILE_JOINED + "java", solution_profile_id: first_gen_id, student_profile_id: second_gen_id, distance: distance)
      return distance
    else
      comp = Comparison.new(profile_type: PROFILE_JOINED + "java", solution_profile_id: first_gen_id, student_profile_id: second_gen_id, distance: distance)
      
      return comp
    end
  end
  
  def distance_measure_c(first_gen_id, first_c_id, second_gen_id, second_c_id, save=true)
    distance = 0.0
    vector_length_1 = 0.0
    vector_length_2 = 0.0
    
    first_gen_profile = GeneralProfile.find(first_gen_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio", limit: 1)
                                          
    first_c_profile = CProfile.find(first_c_id, select: "unary_ratio, short_operator_ratio", limit: 1)
                                          
    second_gen_profile = GeneralProfile.find(second_gen_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio", limit: 1)
    
    second_c_profile = CProfile.find(second_c_id, select: "unary_ratio, short_operator_ratio", limit: 1)
    
    gen_weight_profile = nil
    c_weight_profile = nil
    
    if(WEIGHTENING == true)
      user_gen_profile = GeneralProfile.find(second_gen_id)
      
      gen_weight_profile = GeneralProfile.find(:all, conditions: "user_id = #{user_gen_profile.user_id} AND status = \"weight\"", select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length").first
                                          
      c_weight_profile = CProfile.find(:all, conditions: "user_id = #{user_gen_profile.user_id} AND status = \"weight\"", select: "unary_ratio, short_operator_ratio").first  
    end
    
    first_gen_profile.attributes.each do |metric|
      vector_length_1 += metric[1]
    end
    
    first_c_profile.attributes.each do |metric|
      vector_length_1 += metric[1]
    end
    
    second_gen_profile.attributes.each do |metric|
      vector_length_2 += metric[1]
    end
    
    second_c_profile.attributes.each do |metric|
      vector_length_2 += metric[1]
    end
    
    # for i in(0..first_profile.attributes.values.size-1)
      # distance += ((first_gen_profile.attributes.values[i]/vector_length_1) - (second_gen_profile.attributes.values[i]/vector_length_2)).abs
    # end
    
    # for i in(0..first_profile.attributes.values.size-1)
      # distance += ((first_java_profile.attributes.values[i]/vector_length_1) - (second_java_profile.attributes.values[i]/vector_length_2)).abs
    # end
    
    #without normalization
    if(gen_weight_profile != nil && c_weight_profile != nil)
      for i in(0..first_gen_profile.attributes.values.size-1)
        distance += (first_gen_profile.attributes.values[i] - second_gen_profile.attributes.values[i]).abs * gen_weight_profile.attributes.values[i]
      end
       
      for i in(0..first_c_profile.attributes.values.size-1)
        distance += (first_c_profile.attributes.values[i] - second_c_profile.attributes.values[i]).abs * c_weight_profile.attributes.values[i]
      end
    else
      for i in(0..first_gen_profile.attributes.values.size-1)
        distance += (first_gen_profile.attributes.values[i] - second_gen_profile.attributes.values[i]).abs
      end
       
      for i in(0..first_c_profile.attributes.values.size-1)
        distance += (first_c_profile.attributes.values[i] - second_c_profile.attributes.values[i]).abs
      end
    end
        
    if(save)
      Comparison.create(profile_type: PROFILE_JOINED + "c/c++", solution_profile_id: first_gen_id, student_profile_id: second_gen_id, distance: distance)
      return distance
    else
      comp = Comparison.new(profile_type: PROFILE_JOINED + "c/c++", solution_profile_id: first_gen_id, student_profile_id: second_gen_id, distance: distance)
      
      return comp
    end
  end
  
  def euclidian_distance_java(first_gen_id, first_java_id, second_gen_id, second_java_id, save=true)
    euclidian = 0.0
    vector_length_1 = 0.0
    vector_length_2 = 0.0
    
    first_gen_profile = GeneralProfile.find(first_gen_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length", limit: 1)
                                          
    first_java_profile = JavaProfile.find(first_java_id, select: "javadoc_ratio, try_catch_ratio, throws_ratio", limit: 1)
    
    second_gen_profile = GeneralProfile.find(second_gen_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length", limit: 1)
           
    second_java_profile = JavaProfile.find(second_java_id, select: "javadoc_ratio, try_catch_ratio, throws_ratio", limit: 1)
    
    gen_weight_profile = nil
    java_weight_profile = nil
    
    if(WEIGHTENING == true)
      user_gen_profile = GeneralProfile.find(second_gen_id)
      
      gen_weight_profile = GeneralProfile.find(:all, conditions: "user_id = #{user_gen_profile.user_id} AND status = \"weight\"", select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length").first
                                          
      java_weight_profile = JavaProfile.find(:all, conditions: "user_id = #{user_gen_profile.user_id} AND status = \"weight\"", select: "javadoc_ratio, try_catch_ratio, throws_ratio").first  
    end                                          
    
    first_gen_profile.attributes.each do |metric|
      vector_length_1 += metric[1]**2 #power of 2
    end
    
    first_java_profile.attributes.each do |metric|
      vector_length_1 += metric[1]**2
    end
    
    second_gen_profile.attributes.each do |metric|
      vector_length_2 += metric[1]**2
    end
    
    second_java_profile.attributes.each do |metric|
      vector_length_2 += metric[1]**2
    end
    
    vector_length_1 = Math.sqrt(vector_length_1)
    vector_length_2 = Math.sqrt(vector_length_2)
    
    if(gen_weight_profile != nil && java_weight_profile != nil)
      for i in(0..first_gen_profile.attributes.values.size-1)
        euclidian += (((first_gen_profile.attributes.values[i]/vector_length_1) - (second_gen_profile.attributes.values[i]/vector_length_2))**2) * gen_weight_profile.attributes.values[i]
      end
      
      for i in(0..first_java_profile.attributes.values.size-1)
        euclidian += (((first_java_profile.attributes.values[i]/vector_length_1) - (second_java_profile.attributes.values[i]/vector_length_2))**2) * java_weight_profile.attributes.values[i]
      end
    else
      for i in(0..first_gen_profile.attributes.values.size-1)
        euclidian += ((first_gen_profile.attributes.values[i]/vector_length_1) - (second_gen_profile.attributes.values[i]/vector_length_2))**2
      end
      
      for i in(0..first_java_profile.attributes.values.size-1)
        euclidian += ((first_java_profile.attributes.values[i]/vector_length_1) - (second_java_profile.attributes.values[i]/vector_length_2))**2
      end
    end
    
    euclidian = Math.sqrt(euclidian)
    
    if(save)
      Comparison.create(profile_type: PROFILE_JOINED + "java", solution_profile_id: first_gen_id, student_profile_id: second_gen_id, distance: euclidian)
      return euclidian
    else
      comp = Comparison.new(profile_type: PROFILE_JOINED + "java", solution_profile_id: first_gen_id, student_profile_id: second_gen_id, distance: euclidian)
      return comp
    end
  end
  
  def euclidian_distance_c(first_gen_id, first_c_id, second_gen_id, second_c_id, save=true)
    euclidian = 0.0
    vector_length_1 = 0.0
    vector_length_2 = 0.0
    
    first_gen_profile = GeneralProfile.find(first_gen_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length", limit: 1)
                                          
    first_c_profile = CProfile.find(first_c_id, select: "unary_ratio, short_operator_ratio", limit: 1)
    
    second_gen_profile = GeneralProfile.find(second_gen_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length", limit: 1)
           
    second_c_profile = CProfile.find(second_c_id, select: "unary_ratio, short_operator_ratio", limit: 1)
    
    gen_weight_profile = nil
    c_weight_profile = nil
    
    if(WEIGHTENING == true)
      user_gen_profile = GeneralProfile.find(second_gen_id)
      
      gen_weight_profile = GeneralProfile.find(:all, conditions: "user_id = #{user_gen_profile.user_id} AND status = \"weight\"", select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length").first
                                          
      c_weight_profile = CProfile.find(:all, conditions: "user_id = #{user_gen_profile.user_id} AND status = \"weight\"", select: "unary_ratio, short_operator_ratio").first  
    end                                               
    
    first_gen_profile.attributes.each do |metric|
      vector_length_1 += metric[1]**2 #power of 2
    end
    
    first_c_profile.attributes.each do |metric|
      vector_length_1 += metric[1]**2
    end
    
    second_gen_profile.attributes.each do |metric|
      vector_length_2 += metric[1]**2
    end
    
    second_c_profile.attributes.each do |metric|
      vector_length_2 += metric[1]**2
    end
    
    vector_length_1 = Math.sqrt(vector_length_1)
    vector_length_2 = Math.sqrt(vector_length_2)
    
    if(gen_weight_profile != nil && c_weight_profile != nil)
      for i in(0..first_gen_profile.attributes.values.size-1)
        euclidian += (((first_gen_profile.attributes.values[i]/vector_length_1) - (second_gen_profile.attributes.values[i]/vector_length_2))**2) * gen_weight_profile.attributes.values[i]
      end
      
      for i in(0..first_c_profile.attributes.values.size-1)
        euclidian += (((first_c_profile.attributes.values[i]/vector_length_1) - (second_c_profile.attributes.values[i]/vector_length_2))**2) * c_weight_profile.attributes.values[i]
      end
    else
      for i in(0..first_gen_profile.attributes.values.size-1)
        euclidian += ((first_gen_profile.attributes.values[i]/vector_length_1) - (second_gen_profile.attributes.values[i]/vector_length_2))**2
      end
      
      for i in(0..first_c_profile.attributes.values.size-1)
        euclidian += ((first_c_profile.attributes.values[i]/vector_length_1) - (second_c_profile.attributes.values[i]/vector_length_2))**2
      end
    end
      
    euclidian = Math.sqrt(euclidian)
    
    if(save)
      Comparison.create(profile_type: PROFILE_JOINED + "c/c++", solution_profile_id: first_gen_id, student_profile_id: second_gen_id, distance: euclidian)
      return euclidian
    else
      comp = Comparison.new(profile_type: PROFILE_JOINED + "c/c++", solution_profile_id: first_gen_id, student_profile_id: second_gen_id, distance: euclidian)
      return comp
    end
  end
  
  def cosine_distance_java(first_gen_id, first_java_id, second_gen_id, second_java_id, save=true)
    cosine = 0.0
    vector_length_1 = 0.0
    vector_length_2 = 0.0
    
    first_gen_profile = GeneralProfile.find(first_gen_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length", limit: 1)
                                          
    first_java_profile = JavaProfile.find(first_java_id, select: "javadoc_ratio, try_catch_ratio, throws_ratio", limit: 1)
    
    second_gen_profile = GeneralProfile.find(second_gen_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length", limit: 1)
           
    second_java_profile = JavaProfile.find(second_java_id, select: "javadoc_ratio, try_catch_ratio, throws_ratio", limit: 1)                                          
    
    gen_weight_profile = nil
    java_weight_profile = nil
    
    if(WEIGHTENING == true)
      user_gen_profile = GeneralProfile.find(second_gen_id)
      
      gen_weight_profile = GeneralProfile.find(:all, conditions: "user_id = #{user_gen_profile.user_id} AND status = \"weight\"", select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length").first
                                          
      java_weight_profile = JavaProfile.find(:all, conditions: "user_id = #{user_gen_profile.user_id} AND status = \"weight\"", select: "javadoc_ratio, try_catch_ratio, throws_ratio").first  
    end
    
    first_gen_profile.attributes.each do |metric|
      vector_length_1 += metric[1]**2 #power of 2
    end
    
    first_java_profile.attributes.each do |metric|
      vector_length_1 += metric[1]**2 #power of 2
    end
    
    second_gen_profile.attributes.each do |metric|
      vector_length_2 += metric[1]**2
    end
    
    second_java_profile.attributes.each do |metric|
      vector_length_2 += metric[1]**2
    end
    
    vector_length_1 = Math.sqrt(vector_length_1)
    vector_length_2 = Math.sqrt(vector_length_2)
    
    if(gen_weight_profile != nil && java_weight_profile != nil)
      for i in(0..first_gen_profile.attributes.values.size-1)
        cosine += ((first_gen_profile.attributes.values[i]) * (second_gen_profile.attributes.values[i])) * gen_weight_profile.attributes.values[i]
      end
  
      for i in(0..first_java_profile.attributes.values.size-1)
        cosine += ((first_java_profile.attributes.values[i]) * (second_java_profile.attributes.values[i])) * java_weight_profile.attributes.values[i]
      end
    else
      for i in(0..first_gen_profile.attributes.values.size-1)
        cosine += (first_gen_profile.attributes.values[i]) * (second_gen_profile.attributes.values[i])
      end
  
      for i in(0..first_java_profile.attributes.values.size-1)
        cosine += (first_java_profile.attributes.values[i]) * (second_java_profile.attributes.values[i])
      end
    end
        
    if(cosine > 0)
      cosine = cosine / (vector_length_1 * vector_length_2)
    else
      cosine = 0
    end
    
    if(save)
      Comparison.create(profile_type: PROFILE_JOINED + "java", solution_profile_id: first_gen_id, student_profile_id: second_gen_id, distance: cosine)
      return cosine
    else
      comp = Comparison.new(profile_type: PROFILE_JOINED + "java", solution_profile_id: first_gen_id, student_profile_id: second_gen_id, distance: cosine)
      return comp
    end
  end
  
  def cosine_distance_c(first_gen_id, first_c_id, second_gen_id, second_c_id, save=true)
    cosine = 0.0
    vector_length_1 = 0.0
    vector_length_2 = 0.0
    
    first_gen_profile = GeneralProfile.find(first_gen_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length", limit: 1)
                                          
    first_c_profile = CProfile.find(first_c_id, select: "unary_ratio, short_operator_ratio", limit: 1)
    
    second_gen_profile = GeneralProfile.find(second_gen_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length", limit: 1)
           
    secondc_profile = CProfile.find(second_c_id, select: "unary_ratio, short_operator_ratio", limit: 1)         
    
    gen_weight_profile = nil
    c_weight_profile = nil
    
    if(WEIGHTENING == true)
      user_gen_profile = GeneralProfile.find(second_gen_id)
      
      gen_weight_profile = GeneralProfile.find(:all, conditions: "user_id = #{user_gen_profile.user_id} AND status = \"weight\"", select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio, var_english_ratio,
                                          comm_english_ratio, avg_var_length, avg_fun_name_length").first
                                          
      c_weight_profile = CProfile.find(:all, conditions: "user_id = #{user_gen_profile.user_id} AND status = \"weight\"", select: "unary_ratio, short_operator_ratio").first  
    end                                 
    
    first_gen_profile.attributes.each do |metric|
      vector_length_1 += metric[1]**2 #power of 2
    end
    
    first_c_profile.attributes.each do |metric|
      vector_length_1 += metric[1]**2 #power of 2
    end
    
    second_gen_profile.attributes.each do |metric|
      vector_length_2 += metric[1]**2
    end
    
    second_c_profile.attributes.each do |metric|
      vector_length_2 += metric[1]**2
    end
    
    vector_length_1 = Math.sqrt(vector_length_1)
    vector_length_2 = Math.sqrt(vector_length_2)
    
    if(gen_weight_profile != nil && c_weight_profile != nil)
      for i in(0..first_gen_profile.attributes.values.size-1)
        cosine += ((first_gen_profile.attributes.values[i]) * (second_gen_profile.attributes.values[i])) * gen_weight_profile.attributes.values[i]
      end
  
      for i in(0..first_c_profile.attributes.values.size-1)
        cosine += ((first_c_profile.attributes.values[i]) * (second_c_profile.attributes.values[i])) * c_weight_profile.attributes.values[i]
      end
    else
      for i in(0..first_gen_profile.attributes.values.size-1)
        cosine += (first_gen_profile.attributes.values[i]) * (second_gen_profile.attributes.values[i])
      end
  
      for i in(0..first_c_profile.attributes.values.size-1)
        cosine += (first_c_profile.attributes.values[i]) * (second_c_profile.attributes.values[i])
      end
    end
        
    if(cosine > 0)
      cosine = cosine / (vector_length_1 * vector_length_2)
    else
      cosine = 0
    end
    
    if(save)
      Comparison.create(profile_type: PROFILE_JOINED + "c/c++", solution_profile_id: first_gen_id, student_profile_id: second_gen_id, distance: cosine)
      return cosine
    else
      comp = Comparison.new(profile_type: PROFILE_JOINED + "c/c++", solution_profile_id: first_gen_id, student_profile_id: second_gen_id, distance: cosine)
      return comp
    end
  end
  
  #smaller = more similar
  def self.distance_measure(first_id, second_id, save=true)
    distance = 0.0
    vector_length_1 = 0.0
    vector_length_2 = 0.0
    
    first_profile = GeneralProfile.find(first_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio", limit: 1)
    second_profile = GeneralProfile.find(second_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio", limit: 1)
    
    first_profile.attributes.each do |metric|
      vector_length_1 += metric[1]
    end
    
    second_profile.attributes.each do |metric|
      vector_length_2 += metric[1]
    end
    
    # for i in(0..first_profile.attributes.values.size-1)
      # distance += ((first_profile.attributes.values[i]/vector_length_1) - (second_profile.attributes.values[i]/vector_length_2)).abs
    # end
    
    #without normalization
    for i in(0..first_profile.attributes.values.size-1)
      distance += (first_profile.attributes.values[i] - second_profile.attributes.values[i]).abs
    end
    
    if(save)
      Comparison.create(profile_type: PROFILE_GENERAL, solution_profile_id: first_id, student_profile_id: second_id, distance: distance)
      
      return distance
    else
      comp = Comparison.new(profile_type: PROFILE_GENERAL, solution_profile_id: first_id, student_profile_id: second_id, distance: distance)
      
      return comp
    end
    
  end
  
  def self.euclidian_vector_length(values)
      vector_length = 0.0
      
      values.each do |value|
        vector_length += value**2
      end
      
      vector_length = Math.sqrt(vector_length)
      
      return vector_length
  end
  
  #smaller = more similar
  def self.euclidian_distance(first_id, second_id, save=true)
    euclidian = 0.0
    vector_length_1 = 0.0
    vector_length_2 = 0.0
    
    first_profile = GeneralProfile.find(first_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio", limit: 1)
    second_profile = GeneralProfile.find(second_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio", limit: 1)
    
    first_profile.attributes.each do |metric|
      vector_length_1 += metric[1]**2 #power of 2
    end
    
    second_profile.attributes.each do |metric|
      vector_length_2 += metric[1]**2
    end
    
    vector_length_1 = Math.sqrt(vector_length_1)
    vector_length_2 = Math.sqrt(vector_length_2)
    
    for i in(0..first_profile.attributes.values.size-1)
      euclidian += ((first_profile.attributes.values[i]/vector_length_1) - (second_profile.attributes.values[i]/vector_length_2))**2
    end
    
    euclidian = Math.sqrt(euclidian)
    
    if(save)
      Comparison.create(profile_type: PROFILE_GENERAL, solution_profile_id: first_id, student_profile_id: second_id, distance: euclidian)
      return euclidian
    else
      comp = Comparison.new(profile_type: PROFILE_GENERAL, solution_profile_id: first_id, student_profile_id: second_id, distance: euclidian)
      return comp
    end
  end
  
  # <0,1> 1 for identical vectors
  def self.cosine_distance(first_id, second_id, save=true)
    cosine = 0.0
    vector_length_1 = 0.0
    vector_length_2 = 0.0
    
    first_profile = GeneralProfile.find(first_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio", limit: 1)
    second_profile = GeneralProfile.find(second_id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                          multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio", limit: 1)
    
    first_profile.attributes.each do |metric|
      vector_length_1 += metric[1]**2 #power of 2
    end
    
    second_profile.attributes.each do |metric|
      vector_length_2 += metric[1]**2
    end
    
    vector_length_1 = Math.sqrt(vector_length_1)
    vector_length_2 = Math.sqrt(vector_length_2)
    
    for i in(0..first_profile.attributes.values.size-1)
      cosine += (first_profile.attributes.values[i]) * (second_profile.attributes.values[i])
    end
    
    if(cosine > 0)
      cosine = cosine / (vector_length_1 * vector_length_2)
    else
      cosine = 0
    end
    
    if(save)
      Comparison.create(profile_type: PROFILE_GENERAL, solution_profile_id: first_id, student_profile_id: second_id, distance: cosine)
      return cosine
    else
      comp = Comparison.new(profile_type: PROFILE_GENERAL, solution_profile_id: first_id, student_profile_id: second_id, distance: cosine)
      return comp
    end
  end
  
  # use values from base solutions to determine attribute weights specific to the user
  def compute_weights
    attributes_count = self.attributes.values.size - 3 #without created/updated_at
    
    all_base_profiles = self.user.base_sol_gen_profiles
      
    variances_sum = 0
    
    weight_profile = GeneralProfile.new
    weight_profile.user_id = self.user.id
    weight_profile.status = STATUS_WEIGHT 
    
    for i in(4..attributes_count) #leaving out id, user_id, solution _id and status
      attribute_variance = 0
      base_values_array = []
      
      all_base_profiles.each do |base_profile|
        base_values_array.push(base_profile.attributes.values[i].to_f)
      end
      
      base_values_array.combination(2).to_a.each do |combination|
        attribute_variance += (combination[0] - combination[1]).abs
      end
      
      weight_profile.assign_attributes(self.attributes.keys[i] => attribute_variance)
      variances_sum += attribute_variance
    end
    
   for i in (4..attributes_count)
      weight_profile.assign_attributes(weight_profile.attributes.keys[i] => 1-(weight_profile.attributes.values[i]/variances_sum))
   end
    
    weight_profile.save
  end
  
  def adjust(solution_profile)
    attributes_count = self.attributes.values.size - 3 #without created/updated_at
    
    if(PROFILE_ADJUSTMENT_METHOD == "average_2")
      for i in(4..attributes_count) #leaving out id, user_id, solution _id and status
        if(i == 10 && solution_profile.attributes.values[i] == 0 && solution_profile.attributes.values[i+1] == 0)
          next
        end
        
        if(i == 11 && solution_profile.attributes.values[i] == 0 && solution_profile.attributes.values[i-1] == 0)
          next
        end
        
        if(i == 12 && solution_profile.attributes.values[i] == 0 && solution_profile.attributes.values[i+1] == 0 && solution_profile.attributes.values[i+2] == 0)
          break
        end        #when ratio values are 0 don't adjust with them
        
        sum = self.attributes.values[i] + solution_profile.attributes.values[i] 
        diff = (self.attributes.values[i]/sum - solution_profile.attributes.values[i]/sum).abs
        
        if(diff <= ADJUSTMENT_TRESHOLD)
          self.update_attribute(self.attributes.keys[i], sum/2)
        end
      end
    end
    
    if(PROFILE_ADJUSTMENT_METHOD == "average_all")
      all_sol_profiles = []
      
      #all_sol_profiles = self.user.sol_gen_profiles
      all_sol_profiles = self.user.base_sol_gen_profiles
      
      for i in(4..attributes_count) #leaving out id, user_id, solution _id and status
        sum = 0
        count = all_sol_profiles.length
        
        all_sol_profiles.each do |sol_profile|
          if(i == 10 && sol_profile.attributes.values[i] == 0 && sol_profile.attributes.values[i+1] == 0)
            count -= 1
            next
          end
          
          if(i == 11 && sol_profile.attributes.values[i] == 0 && sol_profile.attributes.values[i-1] == 0)
            count -= 1
            next
          end
          
          if(i == 12 && sol_profile.attributes.values[i] == 0 && sol_profile.attributes.values[i+1] == 0 && sol_profile.attributes.values[i+2] == 0)
            count -= 1
            next
          end        #when ratio values are 0 don't adjust with them
          sum += sol_profile.attributes.values[i]
        end
        if(count > 0)
          self.update_attribute(self.attributes.keys[i], sum/count)
        end
      end
    end
    
    if(PROFILE_ADJUSTMENT_METHOD == "median")
      all_sol_profiles = []
      
      #all_sol_profiles = self.user.base_sol_gen_profiles
      all_sol_profiles = self.user.sol_gen_profiles
      
      
      for i in(4..attributes_count) #leaving out id, user_id, solution _id and status
        sum = 0
        count = all_sol_profiles.length
        values_array = []
                
        all_sol_profiles.each do |sol_profile|
          values_array.push(sol_profile.attributes.values[i].to_f)
        end
        
        length = values_array.length
        sorted = values_array.sort
        median = (length % 2 == 1)? sorted[length/2] : (sorted[length/2-1] + sorted[length/2])/2
        
        self.update_attribute(self.attributes.keys[i], median)
      end
    end
  end
  
  #compares gen profile (of solution) with general profiles of users or their solutions
  #solving the same assignment 
  def compare_with_others(authors_distance)
    PLAGBOOK_LOG.info("Comparing general profile ID " + self.id.to_s)
    
    self_solution = self.solution
    
    if(COMPARE_PROFILES)  #comparing profiles of other students solving the same assignment
      same_assignment_solutions = Solution.find(:all, conditions: "id != #{self_solution.id} AND assignment_id = #{self_solution.assignment_id}",
                                          select: "user_id", group: "user_id")  # initializing a model object with only selected fields
                                                            #group acts like distinct
      ids = ""
      same_assignment_solutions.each do |solution|
        ids += solution.user_id.to_s + ","  #using user IDs
      end
      
      if(ids.length > 0)
        ids = ids[0,ids.size-1] #removing last ,
        gen_profiles = GeneralProfile.find(:all, conditions: "user_id IN (#{ids})")
      end
      
    else  #comparing profiles of solutions of the same assignment
      
      same_assignment_solutions = Solution.find(:all, conditions: "id != #{self_solution.id} AND assignment_id = #{self_solution.assignment_id}",
                                          select: "id", group: "id")  # initializing a model object with only selected fields
                                                       #group acts like distinct
      
      ids = ""
      same_assignment_solutions.each do |solution|
        ids << solution.id.to_s + "," #using solution IDs
      end
      
      if(ids.length > 0)
        ids = ids[0,ids.size-1] #removing last ,
        gen_profiles = GeneralProfile.find(:all, conditions: "solution_id IN (#{ids})")
      end
    end
    
    #COMMON PART
    
    if(gen_profiles)  #there are some profiles to compare  
      comparison_results = []
      
      gen_profiles.each do |profile|
        case COMPARISON_METHOD
          when "distance_measure"
            comparison_results.push(GeneralProfile.distance_measure(self.id, profile.id, false))
          when "euclidian_distance"
            comparison_results.push(GeneralProfile.euclidian_distance(self.id, profile.id, false))
          when "cosine_distance"
            comparison_results.push(GeneralProfile.cosine_distance(self.id, profile.id, false))
        end
      end
      
      if(COMPARISON_METHOD == "cosine_distance")
        comparison_results.sort! { |a,b| b.distance <=> a.distance } #DESC because 1 is value for similarity here
      else
        comparison_results.sort! { |a,b| a.distance <=> b.distance } #ASC because here it's 0
      end
        
      #save only those comparisons which are more similar than solution's author
      comparison_results.each do |comparison|
        save = false
        
        if(COMPARISON_METHOD == "cosine_distance")
          if(comparison.distance > authors_distance)
            save = true
          end
        else
          if(comparison.distance <= authors_distance)
            save = true
          end
        end
        
        if(save)
          if(COMPARE_PROFILES)
            comparison.profile_type = PROFILE_GENERAL
          else
            comparison.profile_type = PROFILE_GENERAL + "-solutions" #means that second profile_id also belongs to solution
          end
          
          self.update_attribute("status", STATUS_SUSPICIOUS)
          #self_solution.update_attribute("suspicious", true)
          
          comparison.save
        else
          break
        end
      end
    end
  end
  
end
