# == Schema Information
#
# Table name: subjects
#
#  id         :integer(4)      not null, primary key
#  title      :string(255)
#  teacher_id :integer(4)
#  created_at :datetime
#  updated_at :datetime
#

class Subject < ActiveRecord::Base
  attr_accessible :title, :teacher_id
  
  has_many :assignments, dependent: :destroy
  belongs_to :teacher, class_name: "User"
  
  validates :teacher_id, presence: true
end

