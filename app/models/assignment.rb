# == Schema Information
#
# Table name: assignments
#
#  id          :integer(4)      not null, primary key
#  name        :string(255)
#  description :text
#  tags        :text
#  subject_id  :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#

class Assignment < ActiveRecord::Base
  attr_accessible :name, :tags, :description, :subject_id
  
  validates :name, presence: true
  
  has_and_belongs_to_many :prog_languages
  belongs_to :subject
  has_many :solutions, dependent: :destroy
end
