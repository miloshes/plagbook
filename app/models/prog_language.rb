# == Schema Information
#
# Table name: prog_languages
#
#  id         :integer(4)      not null, primary key
#  language   :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class ProgLanguage < ActiveRecord::Base
  has_and_belongs_to_many :assignments
  has_many :solutions
end
