class UsersController < ApplicationController
  before_filter :signed_in_user, only: [:index, :edit, :update]
  before_filter :correct_user,   only: [:edit, :update]
  before_filter :admin_user,     only: :destroy
  
  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
    @solutions = @user.solutions.order("created_at DESC").paginate(page: params[:page])
    
    gen_profile = GeneralProfile.find(@user.general_profile.id, select: "id, avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                         multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio", limit: 1)
    
    @gen_attributes_count = gen_profile.attributes.values.size-1
    @gen_attributes_names = gen_profile.attributes.keys
    @gen_values = gen_profile.attributes.values
    
    if(@user.java_profile)
      java_profile = JavaProfile.find(@user.java_profile.id, select: "id, javadoc_ratio, try_catch_ratio,
                                                    throws_ratio", limit: 1)
      
      @java_attributes_count = java_profile.attributes.values.size-1
      @java_attributes_names = java_profile.attributes.keys
      @java_values = java_profile.attributes.values
    end                                     
    
    if(@user.c_profile)
      c_profile = CProfile.find(@user.c_profile.id, select: "id, unary_ratio, short_operator_ratio", limit: 1)
      
      @c_attributes_count = c_profile.attributes.values.size-1
      @c_attributes_names = c_profile.attributes.keys
      @c_values = c_profile.attributes.values
    end
  end
  
  def new
    @user = User.new
    if(current_user && current_user.group_id == 1)
      @root = true
    else
      @root = false
    end
  end
  
  def create
    @user = User.new(params[:user])
    if(!current_user) #when signing up, create new teacher account
      @user.group_id = 2
    else
      if(current_user.group_id == 2)  #teacher can only create new students
        @user.group_id = 3
      end
    end
    
    if @user.save
      if(!current_user)
        sign_in @user
        flash[:success] = "Welcome to the Plagbook!!"
      end
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def edit
  end
  
  def update
    if @user.update_attributes(params[:user])
      flash[:success] = "Profile updated"
      sign_in @user
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User destroyed."
    redirect_to users_path
  end
  
  # GET /users/stats
  # param delete=true will destroy all users with less than highest number of solutions
  def stats
    active_assignments = Solution.find(:all, select: "assignment_id", group: "assignment_id")
    
    @stats = {}
    is_opensource = false
    
    if(active_assignments[0].assignment.id == 18) # opensource
      is_opensource = true
    end  
    
    if(is_opensource)
      @stats["open_source"] = 0
    else
      for i in 0..active_assignments.count
        @stats[i.to_s + "_solutions"] = 0      
      end
    end
    
    User.find(:all, conditions: "group_id = 3").each do |user|
      solutions_count = user.solutions.count

      if(solutions_count)         #don't delete tester
        if(params[:delete] && user.id != 3 && solutions_count < active_assignments.count)
          user.destroy
        else
          if(is_opensource)
            @stats["open_source"] += 1
          else
            @stats[solutions_count.to_s + "_solutions"] += 1
          end
        end
      end
    end
    
    @non_base_solutions = GeneralProfile.where('user_id IS NULL AND (status != "base" OR status IS NULL)').count
        
    @suspicious_count = @non_base_solutions - Solution.where("suspicious = 1").count
    
    @author_second = Solution.where("suspicious = 2").count 
    
    @author_third = Solution.where("suspicious = 3").count     
  end
  
  private

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_path) unless current_user?(@user)
    end
    
    def admin_user
      redirect_to(root_path) unless current_user.group_id == 1
    end
end
