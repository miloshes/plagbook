class GeneralProfilesController < ApplicationController
  
  #PUT /update
  #updates profile values using solution's data
  def update
    gen_profile = GeneralProfile.find(params[:gen_stud_profile_id].to_i)
    params.each do |param|
      if param[0].include? "check"
        checked_values = param[1].split('=',2)
        gen_profile.update_attribute(checked_values[0], checked_values[1])
      end
    end
    
    GeneralProfile.find(params[:gen_sol_profile_id].to_i).update_attribute("status", STATUS_ADDED)
    
    flash.now[:success] = "Profile successfully updated"
    redirect_to user_path(gen_profile.user.id)
  end
  
  def adjust_profile
    @solution = Solution.find(params[:id])

    solution_gen_profile = GeneralProfile.find_by_solution_id(@solution.id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                         multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio")
    
    @solution_gen_profile_id = @solution.general_profile.id
    @attributes_count = solution_gen_profile.attributes.values.size-1
    @attributes_names = solution_gen_profile.attributes.keys
    @solution_values = solution_gen_profile.attributes.values
    
    student_gen_profile = GeneralProfile.find_by_user_id(@solution.user.id, select: "avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                         multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio")
    
    @student_gen_profile_id = @solution.user.general_profile.id
    @student_values = student_gen_profile.attributes.values
    
    #choosing profile adjustment method
    #and computing new values
    if(PROFILE_ADJUSTMENT_METHOD == "average_2")
      @adjusted_values = adjust_with_average_2(@solution_values, @student_values)
    end
    
    if(PROFILE_ADJUSTMENT_METHOD == "average_all")
      @adjusted_values = adjust_with_average_all(@solution.user.id, student_gen_profile)
    end
     
    @deltas = []
    for i in(0..@attributes_count)
      sum = @solution_values[i] + @student_values[i]
      if(sum != 0)
        @deltas.push((@solution_values[i]/sum - @student_values[i]/sum).abs)
      else
        @deltas.push 0
      end
    end
  end
  
  private
  
    def adjust_with_average_2(solution_values, profile_values)
      new_values = []
      
      for i in(0..solution_values.size-1)
        new_values << (solution_values[i] + profile_values[i])/2
      end
      
      return new_values      
    end
    
    def adjust_with_average_all(user_id, stud_gen_profile)
      new_values = []
      all_sol_gen_profiles = []
                                                    #not adjusting with suspicious solutions
      User.find(user_id).solutions.where("suspicious = false").each do |solution|
        all_sol_gen_profiles << solution.general_profile 
      end
      
      for i in(4..stud_gen_profile.attributes.count - 3)
        sum = 0.0
        
        all_sol_gen_profiles.each do |profile|
          sum += profile.attributes.values[i] 
        end
        
        new_values << sum/all_sol_gen_profiles.length
      end
      
      return new_values
    end
end
