class AssignmentsController < ApplicationController
  
  def index
    @assignments = Assignment.paginate(page: params[:page])  
  end
  
  def new
    @assignment = Assignment.new
  end
  
  def create
    @assignment = Assignment.new(params[:assignment])
    
    if @assignment.save
      flash[:success] = "Assignment '#{@assignment.name}' successfuly created."
      redirect_to assignments_path
    else
      render 'new'
    end
  end
end
