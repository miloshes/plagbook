class SubjectsController < ApplicationController
  
  def index
    @subjects = Subject.paginate(page: params[:page])
  end
  
  def new
    @subject = Subject.new
  end
  
  def create
    @subject = Subject.new(params[:subject])
    
    if @subject.save
      flash[:success] = "Subject '#{@subject.title}' created."
      redirect_to subjects_path
    else
      render 'new'
    end
  end
end
