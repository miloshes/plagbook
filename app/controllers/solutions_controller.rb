class SolutionsController < ApplicationController
  before_filter :signed_in_user
  
  require 'iconv'
  
  def new
    @solution = Solution.new
    if(params[:user_id])
      @default = params[:user_id]
    else
      @defalt = 1
    end    
  end
  
  def show
    @solution = Solution.find(params[:id])
    
    #general profile
    solution_gen_profile = GeneralProfile.find(@solution.general_profile.id, select: "id, avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                         multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio,
                                         var_english_ratio, comm_english_ratio, avg_var_length, avg_fun_name_length", limit: 1)
                                         
    @gen_attributes_count = solution_gen_profile.attributes.values.size-1
    @gen_attributes_names = solution_gen_profile.attributes.keys
    @gen_solution_values = solution_gen_profile.attributes.values
    
    if(JOIN_PROFILES == true)
      gen_comparison = Comparison.where(profile_type: PROFILE_JOINED, solution_profile_id: solution_gen_profile.id,
                                      student_profile_id: @solution.user.general_profile.id).first
                                      
      if(@solution.is_suspicious?)
        @gen_chart = []
        comparisons = Comparison.find(:all, conditions: "solution_profile_id = #{@solution.general_profile.id} AND profile_type = " + "\"" + PROFILE_JOINED + @solution.prog_language.language + "\"", order: 'distance ASC')
        comparisons.each do |comparison|
          @gen_chart.push([GeneralProfile.find(comparison.student_profile_id).user, comparison.distance])
        end
      end 
    else
      gen_comparison = Comparison.where(profile_type: PROFILE_GENERAL, solution_profile_id: solution_gen_profile.id,
                                      student_profile_id: @solution.user.general_profile.id).first
    end                                   
                                     
    case @solution.prog_language_id
      when LANG_JAVA
        solution_lang_profile = JavaProfile.find(@solution.java_profile.id, select: "id, javadoc_ratio, try_catch_ratio,
                                                  throws_ratio", limit: 1)
        
        @lang_attributes_count = solution_lang_profile.attributes.values.size-1
        @lang_attributes_names = solution_lang_profile.attributes.keys
        @lang_solution_values = solution_lang_profile.attributes.values
    
        lang_comparison = Comparison.where(profile_type: PROFILE_JAVA, solution_profile_id: solution_lang_profile.id,
                                      student_profile_id: @solution.user.java_profile.id).first
                                      
        if(lang_comparison)
          student_lang_profile = JavaProfile.find(lang_comparison.student_profile_id, select: "id, javadoc_ratio, try_catch_ratio,
                                                  throws_ratio", limit: 1)
          
          @lang_distance = lang_comparison.distance
          @lang_student_values = student_lang_profile.attributes.values
          
          if(COMPARISON_METHOD == "distance_measure")
            @lang_solution_vec_len = JavaProfile.distance_vector_length(@lang_solution_values[1..-1])  #leaving out id
            @lang_student_vec_len = JavaProfile.distance_vector_length(@lang_student_values[1..-1])
          else
            @lang_solution_vec_len = JavaProfile.euclidian_vector_length(@lang_solution_values[1..-1])
            @lang_student_vec_len = JavaProfile.euclidian_vector_length(@lang_student_values[1..-1])
            #hmm sqrt(a) + sqrt(b) != sqrt(a+b)
          end
        end
      when LANG_C
        solution_lang_profile = CProfile.find(@solution.c_profile.id, select: "id, unary_ratio, short_operator_ratio", limit: 1)
        
        @lang_attributes_count = solution_lang_profile.attributes.values.size-1
        @lang_attributes_names = solution_lang_profile.attributes.keys
        @lang_solution_values = solution_lang_profile.attributes.values
        
        lang_comparison = Comparison.where(profile_type: PROFILE_C, solution_profile_id: solution_lang_profile.id,
                                      student_profile_id: @solution.user.c_profile.id).first
                                      
        if(lang_comparison)
          student_lang_profile = CProfile.find(lang_comparison.student_profile_id, select: "id, unary_ratio, short_operator_ratio", limit: 1)
          
          @lang_distance = lang_comparison.distance
          @lang_student_values = student_lang_profile.attributes.values
      
          if(COMPARISON_METHOD == "distance_measure")
            @lang_solution_vec_len = GeneralProfile.distance_vector_length(@lang_solution_values[1..-1])  #leaving out id
            @lang_student_vec_len = GeneralProfile.distance_vector_length(@lang_student_values[1..-1])
          else
            @lang_solution_vec_len = GeneralProfile.euclidian_vector_length(@lang_solution_values[1..-1])
            @lang_student_vec_len = GeneralProfile.euclidian_vector_length(@lang_student_values[1..-1])
            #hmm sqrt(a) + sqrt(b) != sqrt(a+b)
          end
        end
    end
    
    if(gen_comparison)
      student_gen_profile = GeneralProfile.find(gen_comparison.student_profile_id, select: "id, avg_line_length, ws_distribution, words_per_line,
                                         avg_fun_length, avg_comm_length, comm_code_ratio, single_comm_ratio,
                                         multi_comm_ratio, for_loop_ratio, while_loop_ratio, do_loop_ratio,
                                         var_english_ratio, comm_english_ratio, avg_var_length, avg_fun_name_length", limit: 1)
      @gen_distance = gen_comparison.distance
      @gen_student_values = student_gen_profile.attributes.values
      
      if(COMPARISON_METHOD == "distance_measure")
        @gen_solution_vec_len = GeneralProfile.distance_vector_length(@gen_solution_values[1..-1])  #leaving out id
        @gen_student_vec_len = GeneralProfile.distance_vector_length(@gen_student_values[1..-1])
      else
        @gen_solution_vec_len = GeneralProfile.euclidian_vector_length(@gen_solution_values[1..-1])
        @gen_student_vec_len = GeneralProfile.euclidian_vector_length(@gen_student_values[1..-1])
        #hmm sqrt(a) + sqrt(b) != sqrt(a+b)
      end
      
      if(@solution.suspicious)
        if(COMPARE_PROFILES)
          comparisons = Comparison.find(:all, conditions: "profile_type = '#{PROFILE_GENERAL}' AND solution_profile_id = #{solution_gen_profile.id} AND student_profile_id != #{student_gen_profile.id}",
                                        order: 'distance DESC')
          
          if(comparisons.count > 0)
            @gen_chart = []
            comparisons.each do |comparison|
              @gen_chart.push([GeneralProfile.find(comparison.student_profile_id).user, comparison.distance])
            end
          end
          
          case @solution.prog_language_id
            when LANG_JAVA          
              comparisons = Comparison.find(:all, conditions: "profile_type = '#{PROFILE_JAVA}' AND solution_profile_id = #{solution_gen_profile.id} AND student_profile_id != #{student_gen_profile.id}",
                                            order: 'distance DESC')
              if(comparisons.count > 0)
                @java_chart = []
                comparisons.each do |comparison|
                  @java_chart.push([JavaProfile.find(comparison.student_profile_id).user, comparison.distance])
                end
              end
              
            when LANG_C
              comparisons = Comparison.find(:all, conditions: "profile_type = '#{PROFILE_C}' AND solution_profile_id = #{solution_gen_profile.id} AND student_profile_id != #{student_gen_profile.id}",
                                            order: 'distance DESC')
              if(comparisons.count > 0)
                @c_chart = []
                comparisons.each do |comparison|
                  @c_chart.push([CProfile.find(comparison.student_profile_id).user, comparison.distance])
                end
              end
          end
          
        else
           
          comparisons = Comparison.find(:all, conditions: "profile_type = '#{PROFILE_GENERAL+"-solutions"}' AND solution_profile_id = #{solution_gen_profile.id}",
                                        order: 'distance DESC')
                                        
          if(comparisons.count > 0)
            @gen_chart = []
            comparisons.each do |comparison|
              profile = GeneralProfile.find(comparison.student_profile_id)
              @gen_chart.push([profile.solution.user, profile.solution, comparison.distance])
            end
          end
          
          case @solution.prog_language_id
          when LANG_JAVA
            comparisons = Comparison.find(:all, conditions: "profile_type = '#{PROFILE_JAVA+"-solutions"}' AND solution_profile_id = #{solution_gen_profile.id}",
                                          order: 'distance DESC')
                                          
            if(comparisons.count > 0)
              @java_chart = []
              comparisons.each do |comparison|
                profile = JavaProfile.find(comparison.student_profile_id)
                @java_chart.push([profile.solution.user, profile.solution, comparison.distance])
              end
            end
          when LANG_C
            comparisons = Comparison.find(:all, conditions: "profile_type = '#{PROFILE_C+"-solutions"}' AND solution_profile_id = #{solution_gen_profile.id}",
                                          order: 'distance DESC')
                                          
            if(comparisons.count > 0)
              @c_chart = []
              comparisons.each do |comparison|
                profile = CProfile.find(comparison.student_profile_id)
                @c_chart.push([profile.solution.user, profile.solution, comparison.distance])
              end
            end
          end
        end
      end
    end
    
    if(@solution.suspicious)
      flash.now[:error] = "SUSPICIOUS SOLUTION"
    end
    
    # case @solution.status
      # when STATUS_BASE
         # flash.now[:notice] = "BASE SOLUTION"
      # when STATUS_ADDED
         # flash.now[:notice] = "ADDED TO PROFILE"
      # when STATUS_SUSPICIOUS
         # flash.now[:error] = "SUSPICIOUS SOLUTION"
    # end
  end

  def create
    #when there are multiple files selected
    if(params[:solution][:filename].size > 1)
      solution_counter = 0
      new_users_counter = 0
      suspicious_solutions = [] #to store suspicious solution so they could be compared after all are uploaded
        
      params[:solution][:filename].each do |file|
        #get student's name from filename, xba_2.cpp will return xba
        user_name = file.original_filename[/^.*?(?=[_\-\.])/]
        
        #try to find user in databse
        user = User.find(:first, conditions: ["lower(last_name) = ?", user_name.downcase])
        
        if(!user)
          user = User.new(last_name: user_name, group_id: 3)
          user.save(validate: false)
          new_users_counter += 1
        end
        
        solution = user.solutions.build(assignment_id: params[:solution][:assignment].to_i, prog_language_id: params[:solution][:prog_language].to_i, 
                                        filename: file.original_filename)
        
        #open file and save it's content to string
        tmp = file.tempfile

        if(params[:solution][:prog_language] == LANG_JAVA)
          # call external jar file to extract variable and function names from source code
          # output is in format "var 1 var 2 | fun1 fun2"
          names = `java -jar ./vendor/tokenizer.jar #{tmp.path}`
          solution.variables = names.split("|")[0].strip
          solution.fun_names = names.split("|")[1].strip
        else
          solution.variables = ""
          solution.fun_names = ""
        end
        
        #we might be able to use only tmp file, need to test it on deployment server
        filepath = File.join("tmp", file.original_filename)
        FileUtils.cp tmp.path, filepath
        
        file = File.open(filepath, "r")
        source_code = ""      
        
        #FIXME when there is weird character in the text due to encoding issues, I skip it 
        #FIXME I asume all files are in ASCII !!
        
        while (line = file.gets)  #to          #from
          line = Iconv.iconv('UTF-8//IGNORE', 'ascii', line).first
          source_code << line 
        end
        
        solution.source_code = source_code
        
        file.close
        File.delete(filepath)
        
        if solution.save
          solution_counter += 1
        
          #generate profiles
          comparison_result = solution.create_profiles
          puts "working on " + solution.id.to_s
          
          #length is null when added solution is base
          if(comparison_result.length > 0)
            suspicious_solutions.push(comparison_result)    #compare with all others
          end        
        end
      end
      
      #compare all suspicious solutions with other student's
      suspicious_solutions.each do |array| #array [[prof, dist],[prof, dist]]
        array.each do |suspicious|         #suspicious [prof, dist]
          if(suspicious[0].class.to_s == "GeneralProfile")
            puts "comparing general profile " + suspicious[0].id.to_s
            suspicious[0].compare_with_others(suspicious[1])
          end
          # if(suspicious[0].class.to_s == "JavaProfile")
            # puts "comparing java profile " + suspicious[0].id.to_s
          # end
          # if(suspicious[0].class.to_s == "CProfile")
            # puts "comparing C profile " + suspicious[0].id.to_s
          # end
          
            
        end
      end
      
      PLAGBOOK_LOG.info(solution_counter.to_s + " solutions added")
      PLAGBOOK_LOG.info(new_users_counter.to_s + " new users added")
      
      flash.now[:success] = solution_counter.to_s + " solutions added!"
      flash.now[:success] = new_users_counter.to_s + " new users added!"
      if(params[:solution][:filename].size > solution_counter)
        flash[:error] = params[:solution][:filename].size - solution_counter + " solutions failed to be saved"
      end
      
      render 'static_pages/home'
      
    else  #only 1 file was submitted
      
      filename = params[:solution][:filename][0]
      
      user = User.find(params[:solution][:user].to_i)
      solution = user.solutions.build(assignment_id: params[:solution][:assignment].to_i, prog_language_id: params[:solution][:prog_language].to_i,
                                      filename: filename.original_filename)
      
      #open file and save it's content to string
      tmp = filename.tempfile
      
      # call external jar file to extract variable and function names from source code
      # output is in format "var 1 var 2 | fun1 fun2"
      names = `java -jar ./vendor/tokenizer.jar #{tmp.path}`
      solution.variables = names.split("|")[0].strip
      solution.fun_names = names.split("|")[1].strip
      
      #we might be able to use only tmp file, need to test it on deployment server
      filepath = File.join("tmp", filename.original_filename)
      FileUtils.cp tmp.path, filepath
      
      file = File.open(filepath, "r")
      source_code = ""      
      
      #FIXME when there is weird character in the text due to encoding issues, I skip it 
      
      while (line = file.gets)  #to          #from
        line = Iconv.iconv('UTF-8//IGNORE', 'ascii', line).first
        source_code << line 
      end
      
      solution.source_code = source_code
      
      file.close
      File.delete(filepath)  
      
      if solution.save
        #generate profiles
        comparison_result = solution.create_profiles
        
        #compare suspicious profiles with other students
        comparison_result.each do |profile, dist|
          profile.compare_with_others(dist)
        end
        
        flash[:success] = "Solution '#{solution.filename}' added!"
        redirect_to solution_path(solution)
      else
        flash.now[:error] = "Failed to save solution"
        render 'static_pages/home'
      end
    end
  end

  def suspicious
    @suspicious_solutions = Solution.find(:all, conditions: "suspicious = true")
  end
  
  def delete_all
    if(params[:id])
      User.find(params[:id]).solutions.each do |solution|
        solution.destroy
      end
      
      g= GeneralProfile.find_by_user_id(params[:id])
      if(g)
        g.delete
      end
      j = JavaProfile.find_by_user_id(params[:id])
      if(j)
        j.delete
      end
      c = CProfile.find_by_user_id(params[:id])
      if(c)
        c.delete
      end       
    else
      User.find(:all, conditions: "id > 3").each do |user|
        user.destroy
      end
      Comparison.delete_all
    end
    
    flash.now[:success] = "All data deleted"
    render 'static_pages/home'
  end
  
  def recompute
    if(GeneralProfile.count > 0)
      GeneralProfile.delete_all
      JavaProfile.delete_all
      # CProfile.delete_all
      
      Comparison.delete_all
    end
    
    Solution.all.each do |solution|
      puts "working on solution " + solution.id.to_s
      solution.update_attribute("suspicious", false)
      solution.build_profiles
    end
    
    User.all.each do |user|
      if(user.solutions.count > 0)
        puts "working on student " + user.id.to_s
        user.build_profiles
        
        if(WEIGHTENING == true)
          user.general_profile.compute_weights
          if(user.java_profile != nil)
            user.java_profile.compute_weights
          end
          if(user.c_profile != nil)
            user.c_profile.compute_weights
          end
        end
      end
    end
    
    puts "all profiles created"
   
    Solution.all.each do |solution|
      if(solution.general_profile.status != STATUS_BASE)  # skip base solutions
        solution.compare_profiles
        puts "comparing solution " + solution.id.to_s
        
        if(solution.is_suspicious?)
          #solution.update_attribute("suspicious", true)
          solution.general_profile.update_attribute("status", STATUS_SUSPICIOUS)
          if(JOIN_PROFILES == true)
            case solution.prog_language_id
            
            when PROFILE_JAVA
              solution.java_profile.update_attribute("status", STATUS_SUSPICIOUS)
            
            when PROFILE_C
              solution.c_profile.update_attribute("status", STATUS_SUSPICIOUS)
            end
          end
        end
      end 
    end
    
    
    
    # #compare all suspicious solutions with other student's
    # suspicious_solutions.each do |array| #array [[prof, dist],[prof, dist]]
      # array.each do |suspicious|         #suspicious [prof, dist]
        # if(suspicious[0].class.to_s == "GeneralProfile")
          # puts "comparing general profile " + suspicious[0].id.to_s
          # suspicious[0].compare_with_others(suspicious[1])
        # end
        # if(suspicious[0].class.to_s == "JavaProfile")
          # puts "comparing java profile " + suspicious[0].id.to_s
          # suspicious[0].compare_with_others(suspicious[1])
        # end
     # # if(suspicious[0].class.to_s == "CProfile")
    # # puts "comparing C profile " + suspicious[0].id.to_s
     # # end
     # end
   # end
   
   redirect_to "/users/stats"    
  end
  
end