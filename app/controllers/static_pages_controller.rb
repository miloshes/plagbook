class StaticPagesController < ApplicationController
  def home
    @solution = current_user.solutions.build if signed_in?
  end

  def help
  end
  
  def about
  end
end
