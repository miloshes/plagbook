class CreateJavaProfiles < ActiveRecord::Migration
  def change
    create_table :java_profiles do |t|
      t.references :user
      t.references :solution
      t.string :status
      t.float :javadoc_ratio
      t.float :try_catch_ratio
      t.float :throws_ratio

      t.timestamps
    end
    add_index :java_profiles, :user_id
    add_index :java_profiles, :solution_id
  end
end
