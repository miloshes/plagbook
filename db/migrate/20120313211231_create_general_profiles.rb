class CreateGeneralProfiles < ActiveRecord::Migration
  def change
    create_table :general_profiles do |t|
      t.references :user
      t.references :solution
      t.string :status
      t.float :avg_line_length
      t.float :ws_distribution
      t.float :words_per_line
      t.float :avg_fun_length
      t.float :avg_comm_length
      t.float :comm_code_ratio
      t.float :single_comm_ratio
      t.float :multi_comm_ratio
      t.float :for_loop_ratio
      t.float :while_loop_ratio
      t.float :do_loop_ratio

      t.timestamps
    end
    add_index :general_profiles, :user_id
    add_index :general_profiles, :solution_id
  end
end
