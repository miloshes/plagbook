class CreateCProfiles < ActiveRecord::Migration
  def change
    create_table :c_profiles do |t|
      t.references :user
      t.references :solution
      t.string :status
      t.float :unary_ratio
      t.float :short_operator_ratio
      
      t.timestamps
    end
    add_index :c_profiles, :user_id
    add_index :c_profiles, :solution_id
  end
end
