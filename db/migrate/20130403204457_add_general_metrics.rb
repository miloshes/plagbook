class AddGeneralMetrics < ActiveRecord::Migration
  def change
    add_column :general_profiles, :comm_english_ratio, :float
    add_column :general_profiles, :var_english_ratio, :float
    add_column :general_profiles, :avg_var_length, :float
    add_column :general_profiles, :avg_fun_name_length, :float
  end
end
