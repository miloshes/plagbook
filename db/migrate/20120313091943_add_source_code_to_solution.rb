class AddSourceCodeToSolution < ActiveRecord::Migration
  def change
    add_column :solutions, :source_code, :text
    rename_column :solutions, :filepath, :filename
  end
end
