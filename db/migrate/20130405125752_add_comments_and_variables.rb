class AddCommentsAndVariables < ActiveRecord::Migration
  def change
    add_column :solutions, :code, :text
    add_column :solutions, :comments, :text
    add_column :solutions, :variables, :text
    add_column :solutions, :fun_names, :text
  end 
end
