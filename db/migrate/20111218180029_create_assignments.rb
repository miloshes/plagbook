class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.string :name
      t.text :description
      t.text :tags
      t.references :subject

      t.timestamps
    end
    add_index :assignments, :subject_id
  end
end
