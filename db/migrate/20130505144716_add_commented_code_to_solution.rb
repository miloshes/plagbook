class AddCommentedCodeToSolution < ActiveRecord::Migration
  def change
    add_column :solutions, :commented_code, :text
  end
end
