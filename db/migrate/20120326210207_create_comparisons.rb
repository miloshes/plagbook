class CreateComparisons < ActiveRecord::Migration
  def change
    create_table :comparisons do |t|
      t.string :profile_type
      t.integer :solution_profile_id
      t.integer :student_profile_id
      t.float :distance

      t.timestamps
    end
    add_index :comparisons, :solution_profile_id
    add_index :comparisons, :student_profile_id
  end
end
