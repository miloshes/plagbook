class CreateProgLanguages < ActiveRecord::Migration
  def change
    create_table :prog_languages do |t|
      t.string :language

      t.timestamps
    end
  end
end
