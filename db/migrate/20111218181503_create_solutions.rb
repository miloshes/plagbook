class CreateSolutions < ActiveRecord::Migration
  def change
    create_table :solutions do |t|
      t.text :filepath
      t.references :assignment
      t.references :prog_language
      t.references :user
      t.boolean :suspicious
      

      t.timestamps
    end
    add_index :solutions, :assignment_id
    add_index :solutions, [:user_id, :created_at]
  end
end
