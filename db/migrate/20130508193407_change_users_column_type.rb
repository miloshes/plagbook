class ChangeUsersColumnType < ActiveRecord::Migration
  def change
    change_column :solutions, :suspicious, :integer
  end
end
