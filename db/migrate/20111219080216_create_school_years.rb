class CreateSchoolYears < ActiveRecord::Migration
  def change
    create_table :school_years do |t|
      t.integer :year
      t.references :user

      t.timestamps
    end
    add_index :school_years, :user_id
  end
end
