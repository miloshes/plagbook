# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

["root", "teacher", "student"].each do |group|
  Group.find_or_create_by_name(group)
end

["java", "c/c++"].each do |language|
  ProgLanguage.find_or_create_by_language(language)
end

User.create(first_name: "root", last_name: "1", login: "root123", password: "root123", password_confirmation: "root123", group_id: 1)
User.create(first_name: "ucitel", last_name: "_", login: "ucitel", password: "ucitel", password_confirmation: "ucitel", group_id: 2)
User.create(first_name: "tester", last_name: "_", login: "test123", password: "test123", password_confirmation: "test123", group_id: 3)

Subject.create(title: "Proceduralne programovanie", teacher_id: 2)
Subject.create(title: "Objektovo-orientovane programovanie", teacher_id: 2)
Subject.create(title: "Datove struktury a algoritmy", teacher_id: 2)
Subject.create(title: "Pocitacove siete", teacher_id: 2)
Subject.create(title: "Interakcia cloveka s pocitacom", teacher_id: 2)
Subject.create(title: "Evolucne algoritmy", teacher_id: 2)
Subject.create(title: "Tvorba efektivnych algoritmov a programov", teacher_id: 2)
Subject.create(title: "Umela inteligencia", teacher_id: 2)
Subject.create(title: "Vyvoj programov na platforme java", teacher_id: 2)
Subject.create(title: "Open-source", teacher_id: 2)

Assignment.create(name: "Projekt 1", tags: "file I/O, struct", subject_id: 1)
Assignment.create(name: "Projekt 2", tags: "pointers, alloc, list", subject_id: 1)
Assignment.create(name: "Semestralny projekt", tags: "OOP fundamentals, inheritance, polymorphism, interface", subject_id: 2)
Assignment.create(name: "Zadanie 1", tags: "", subject_id: 3)
Assignment.create(name: "Zadanie 2", tags: "", subject_id: 3)
Assignment.create(name: "Zadanie 3", tags: "", subject_id: 3)
Assignment.create(name: "Analyzator paketov", tags: "TCP/IP, packet analysis", subject_id: 4)
Assignment.create(name: "UDP transfer", tags: "UDP, data transfer, P2P", subject_id: 4)
Assignment.create(name: "Semestralny projekt", tags: "HCI, UX, UI", subject_id: 5)
Assignment.create(name: "Zadanie 1", tags: "greedy algorithm", subject_id: 7)
Assignment.create(name: "Zadanie 2", tags: "dynamic programming", subject_id: 7)
Assignment.create(name: "Zadanie 3", tags: "divide and conquer, flow network", subject_id: 7)
Assignment.create(name: "Semestralny projekt", tags: "advanced java, multi-threading, logging, collections, web", subject_id: 9)
Assignment.create(name: "Zadanie 2", tags: "deep search", subject_id: 8)
Assignment.create(name: "Zadanie 3", tags: "evolution algorithm, population, fitness", subject_id: 8)
Assignment.create(name: "Zadanie 4", tags: "production system", subject_id: 8)
Assignment.create(name: "Case study", tags: "evolution algorithm, population, fitness", subject_id: 6)
Assignment.create(name: "Open-source", tags: "commercial, free, public", subject_id: 10)





