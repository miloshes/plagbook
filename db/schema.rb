# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130508193407) do

  create_table "admins", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true
  add_index "admins", ["reset_password_token"], :name => "index_admins_on_reset_password_token", :unique => true

  create_table "assignments", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "tags"
    t.integer  "subject_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "assignments", ["subject_id"], :name => "index_assignments_on_subject_id"

  create_table "c_profiles", :force => true do |t|
    t.integer  "user_id"
    t.integer  "solution_id"
    t.string   "status"
    t.float    "unary_ratio"
    t.float    "short_operator_ratio"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "c_profiles", ["solution_id"], :name => "index_c_profiles_on_solution_id"
  add_index "c_profiles", ["user_id"], :name => "index_c_profiles_on_user_id"

  create_table "comparisons", :force => true do |t|
    t.string   "profile_type"
    t.integer  "solution_profile_id"
    t.integer  "student_profile_id"
    t.float    "distance"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "comparisons", ["solution_profile_id"], :name => "index_comparisons_on_solution_profile_id"
  add_index "comparisons", ["student_profile_id"], :name => "index_comparisons_on_student_profile_id"

  create_table "general_profiles", :force => true do |t|
    t.integer  "user_id"
    t.integer  "solution_id"
    t.string   "status"
    t.float    "avg_line_length"
    t.float    "ws_distribution"
    t.float    "words_per_line"
    t.float    "avg_fun_length"
    t.float    "avg_comm_length"
    t.float    "comm_code_ratio"
    t.float    "single_comm_ratio"
    t.float    "multi_comm_ratio"
    t.float    "for_loop_ratio"
    t.float    "while_loop_ratio"
    t.float    "do_loop_ratio"
    t.float    "var_english_ratio"
    t.float    "comm_english_ratio"
    t.float    "avg_var_length"
    t.float    "avg_fun_name_length"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "general_profiles", ["solution_id"], :name => "index_general_profiles_on_solution_id"
  add_index "general_profiles", ["user_id"], :name => "index_general_profiles_on_user_id"

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "java_profiles", :force => true do |t|
    t.integer  "user_id"
    t.integer  "solution_id"
    t.string   "status"
    t.float    "javadoc_ratio"
    t.float    "try_catch_ratio"
    t.float    "throws_ratio"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "java_profiles", ["solution_id"], :name => "index_java_profiles_on_solution_id"
  add_index "java_profiles", ["user_id"], :name => "index_java_profiles_on_user_id"

  create_table "prog_languages", :force => true do |t|
    t.string   "language"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "rails_admin_histories", :force => true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      :limit => 2
    t.integer  "year",       :limit => 8
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], :name => "index_rails_admin_histories"

  create_table "school_years", :force => true do |t|
    t.integer  "year"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "school_years", ["user_id"], :name => "index_school_years_on_user_id"

  create_table "solutions", :force => true do |t|
    t.text     "filename"
    t.integer  "assignment_id"
    t.integer  "prog_language_id"
    t.integer  "user_id"
    t.integer  "suspicious"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.text     "source_code"
    t.text     "code"
    t.text     "comments"
    t.text     "variables"
    t.text     "fun_names"
    t.text     "commented_code"
  end

  add_index "solutions", ["assignment_id"], :name => "index_solutions_on_assignment_id"
  add_index "solutions", ["user_id", "created_at"], :name => "index_solutions_on_user_id_and_created_at"

  create_table "subjects", :force => true do |t|
    t.string   "title"
    t.integer  "teacher_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "login"
    t.string   "password_digest"
    t.integer  "group_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "remember_token"
  end

  add_index "users", ["group_id"], :name => "index_users_on_group_id"
  add_index "users", ["login"], :name => "index_users_on_login", :unique => true
  add_index "users", ["remember_token"], :name => "index_users_on_remember_token"

end
