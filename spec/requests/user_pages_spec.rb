require 'spec_helper'

describe "UserPages" do
  
  subject { page }
  
  describe "GET /signup" do
    before { visit signup_path }
    
    it { should have_selector('title', text: 'Sign up') }
  end
  
  describe "index" do
    let(:user) { FactoryGirl.create(:user) }

    before do
      sign_in user
      visit users_path
    end

    it { should have_selector('title', text: 'All users') }

    describe "pagination" do
      before(:all) { 30.times { FactoryGirl.create(:user) } }
      after(:all)  { User.delete_all }

      it { should have_selector("a", content: 'Next') }
      it { should have_selector("a", content: '2') }

      it "should list each user" do
        User.all[0..2].each do |user|
          should have_selector('li', content: "#{user.first_name} #{user.last_name}")
        end
      end
      
      it { should_not have_link('delete') }
      
      describe "as an root user" do
        let(:admin) { FactoryGirl.create(:root) }
        before do
          sign_in admin
          visit users_path
        end

        it { should have_link('delete', href: user_path(User.first)) }
        it "should be able to delete another user" do
          expect { click_link('delete') }.to change(User, :count).by(-1)
        end
        it { should_not have_link('delete', href: user_path(admin)) }
      end
    end
  end
  
  describe "profile page" do
    let(:user) { FactoryGirl.create(:user) }
    let!(:s1) { FactoryGirl.create(:solution, user: user, filename: "Foo") }
    let!(:s2) { FactoryGirl.create(:solution, user: user, filename: "Bar") }
    
    before { visit user_path(user) }

    it { should have_selector('title', text: user.full_name) }
    
    describe "solutions" do
      it { should have_content(s1.assignment.name) }
      it { should have_content(s2.assignment.name) }
      it { should have_content(user.solutions.count) }
    end
  end
  
  describe "signup" do

    before { visit signup_path }

    describe "with invalid information" do
      it "should not create a user" do
        expect { click_button "Sign up" }.not_to change(User, :count)
      end
    end

    describe "with valid information" do
      before do
        fill_in "First name",   with: "Example Name"
        fill_in "Last name",    with: "Example Surname"
        fill_in "Login",        with: "user@example"
        fill_in "Password",     with: "foobar"
        fill_in "Confirmation", with: "foobar"
      end

      it "should create a user" do
        expect { click_button "Sign up" }.to change(User, :count).by(1)
        
        should have_link('Sign out', href: signout_path)
      end
    end
  end
  
  describe "edit" do
    let(:user) { FactoryGirl.create(:user) }
    before do
      sign_in user
      visit edit_user_path(user)
    end

    describe "page" do
      it { should have_selector('title', text: "Edit user") }
    end

    describe "with invalid information" do
      let(:error) { '1 error prohibited this user from being saved' }
      before { click_button "Update" }

      it { should have_content(error) }
    end
   
    describe "with valid information" do
      let(:new_f_name)  { "New" }
      let(:new_l_name)  { "Name" }
      let(:new_login) { "tester" }
      before do
        fill_in "First name",   with: new_f_name
        fill_in "Last name",    with: new_l_name
        fill_in "Login",        with: new_login
        fill_in "Password",     with: user.password
        fill_in "Confirmation", with: user.password
        click_button "Update"
      end
      
      it { should have_selector('title', text: "#{new_f_name} #{new_l_name}") }
      it { should have_selector('div.flash.success') }
      it { should have_link('Sign out', href: signout_path) }
      specify { user.reload.first_name.should  == new_f_name }
      specify { user.reload.login.should == new_login }
    end
  end
end