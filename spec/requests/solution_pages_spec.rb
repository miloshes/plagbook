require 'spec_helper'

describe "SolutionPages" do
  let!(:assignment1) {FactoryGirl.create(:assignment, name: "first") }
  let!(:assignment2) {FactoryGirl.create(:assignment, name: "second") }
  let!(:student1) { FactoryGirl.create(:student, first_name: "a", last_name: "b") }
  let!(:student2) { FactoryGirl.create(:student, first_name: "c", last_name: "d") }
  
  subject { page }
  
  describe "create solution" do
    let(:teacher) { FactoryGirl.create(:teacher) }
    before do
        sign_in teacher
        visit "/solutions/new"
    end
    
    describe "after selecting values and file" do
      
      before do
        select "c d", from: "solution_user" 
        select "second", from: "solution_assignment"
        attach_file("solution_filename", 'C:/Rubydev/test.txt')
      end
       
      it "should create new solution record" do
        expect { click_button "Upload" }.to change(Solution, :count).by(1)
        
        should have_selector('div.flash.success')
      end
      
      describe "duplicating profiles" do
        it "should use solution's profile also for user when this is her first solution" do
           expect { click_button "Upload" }.to change(GeneralProfile, :count).by(2)
           
           should have_selector('div.flash.success')
        end
        
        it "should redirect to new solution detail showing only solution's metrics" do
          click_button "Upload"
          
          should have_content("General profile")
          should have_content("avg_line_length")
          should have_content("33.38095238095238")
        end        
      end
      
      describe "comparing profiles" do
        before do
          FactoryGirl.create(:general_profile, user_id: student2.id)
        end
        
        it "should create new comparison record when user has profile already" do
          expect { click_button "Upload" }.to change(Comparison, :count).by(1)
        end
        
        it "should redirect to new solution detail showing comparison" do
          click_button "Upload"
          
          should have_content("General profiles overall comparison")
          should have_content("avg_line_length")
          should have_content("33.38095238095238")
        end
      end
    end
  end
end