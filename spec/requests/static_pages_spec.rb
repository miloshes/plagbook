require 'spec_helper'

describe "Static Pages" do
  
  let(:base_title) { "Plagbook" }
  subject { page }
  
  describe "Home page" do
    before { visit root_path } 
    
    it { should have_selector('title', text: full_title('Home')) }
  end
  
  describe "Help page" do
    before { visit help_path } 
    
    it { should have_selector('title', text: full_title('Help')) }
  end
  
  describe "About page" do
     before { visit about_path } 
     
     it { should have_selector('title', text: full_title('About')) }
  end
  
  describe "Sign up page" do
     before { visit signup_path } 
     
     it { should have_selector('title', text: full_title('Sign up')) }
  end
  
  # it "should have the right links on the layout" do
    # visit root_path
    # click_link "About"
    # page.should have_selector 'title', text: full_title('About')
    # click_link "Help"
    # page.should have_selector 'title', text: full_title('Help')
    # click_link "Home"
    # page.should have_selector 'title', text: full_title('Home')
    # click_link "Sign up now!"
    # page.should have_selector 'title', text: full_title('Sign up')
  # end
end
