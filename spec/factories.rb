FactoryGirl.define do
  factory :user do
    sequence(:first_name)   { |n| "Name#{n}" }
    sequence(:last_name)	  { |n| "Surname#{n}" }
    sequence(:login)   	    { |n| "person_#{n}@example.com" }
    password 	   "root123"
    
    factory :root do
      group_id 1
    end
    
    factory :teacher do
      group_id 2
    end
    
    factory :student do
      group_id 3
    end
  end
  
  factory :subject do
    title "Java"
    teacher_id 2  
  end
  
  factory :assignment do
    name "basics"
    tags "variables, if"
    description "for noobs"
    subject
  end
  
  factory :solution do
    filename "/file.java"
    user
    assignment
  end
  
  factory :general_profile do
    avg_line_length { rand(10) }
    ws_distribution { rand(10) } 
    words_per_line { rand(10) } 
    avg_fun_length { rand(10) } 
    avg_comm_length { rand(10) } 
    comm_code_ratio { rand(10) } 
    single_comm_ratio { rand(10) } 
    multi_comm_ratio { rand(10) } 
    for_loop_ratio { rand(10) } 
    while_loop_ratio { rand(10) } 
    do_loop_ratio { rand(10) }
    
    factory :solution_gen_prof do
      solution
    end
     
    factory :user_gen_prof do
      user
    end 
  end
end