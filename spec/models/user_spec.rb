require 'spec_helper'

describe User do

  before { @user = User.new(first_name: "Example name", last_name: "Example surname",
                            login: "example_login", password: "foobar", password_confirmation: "foobar") }

  subject { @user }

  it { should respond_to(:first_name) }
  it { should respond_to(:last_name) }
  it { should respond_to(:login) }
  it { should respond_to(:password_digest) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:remember_token) }
  
  it { should respond_to(:authenticate) }
  it { should respond_to(:solutions) }
  
  it { should be_valid }

  describe "when first name is not present" do
    before { @user.first_name = " " }
    it { should_not be_valid }
  end
  
  describe "when last name is not present" do
    before { @user.last_name = " " }
    it { should_not be_valid }
  end
  
  describe "when login is not present" do
    before { @user.login = " " }
    it { should_not be_valid }
  end
  
  describe "when password is not present" do
    before { @user.password = @user.password_confirmation = " " }
    it { should_not be_valid }
  end
  
  describe "when login is too long" do
    before { @user.login = "a" * 51 }
    it { should_not be_valid }
  end
  
  describe "when login is already taken" do
    before do
      user_with_same_login = @user.dup
      user_with_same_login.login = @user.login.upcase
      user_with_same_login.save
    end

    it { should_not be_valid }
  end
  
  describe "when password doesn't match confirmation" do
    before { @user.password_confirmation = "mismatch" }
    it { should_not be_valid }
  end
  
  describe "with a password that's too short" do
    before { @user.password = @user.password_confirmation = "a" * 5 }
    it { should be_invalid }
  end
  
  describe "return value of authenticate method" do
    before { @user.save }
    let(:found_user) { User.find_by_login(@user.login) }

    describe "with valid password" do
     it { should == found_user.authenticate(@user.password) }
    end
  
    describe "with invalid password" do
      let(:user_for_invalid_password) { found_user.authenticate("invalid") }

      it { should_not == user_for_invalid_password }
      specify { user_for_invalid_password.should be_false }
    end
  end
  
  describe "remember token" do
    before { @user.save }
    its(:remember_token) { should_not be_blank }
  end
  
  describe "solution associations" do
    before { @user.save }
    let!(:older_solution) do 
      FactoryGirl.create(:solution, user: @user, created_at: 1.day.ago)
    end
    let!(:newer_solution) do
      FactoryGirl.create(:solution, user: @user, created_at: 1.hour.ago)
    end

    it "should have the right solution in the right order" do
      @user.solutions.should == [newer_solution, older_solution]
    end
    
    it "should destroy associated solutions" do
      solutions = @user.solutions
      @user.destroy
      solutions.each do |solution|
        Solution.find_by_id(solution.id).should be_nil
      end
    end
  end
end