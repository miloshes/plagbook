require 'spec_helper'

describe Group do
  before do
    @group = Group.create(name: "Teacher")
  end
  
  subject { @group }
  
  it { should respond_to(:name) }
  
  it { should respond_to(:users) }
  
  it { should be_valid }
end