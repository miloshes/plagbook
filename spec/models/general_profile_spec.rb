require 'spec_helper'

describe GeneralProfile do
  let!(:profile1) { FactoryGirl.create(:general_profile) }
  let!(:profile2) { FactoryGirl.create(:general_profile) }
  
  describe "distance measure" do
    
    it "distance should be a number < 1 and > 0" do
      @distance = GeneralProfile.distance_measure(1, 2)
      
      #not sure if 'be' works
      @distance.should be < 1
      @distance.should be > 0  
    end
    
    it "should create new comparison record" do
      expect { GeneralProfile.distance_measure(1, 2) }.to change(Comparison, :count).by(1)
    end
  end
  
  describe "euclidian distance" do
    
    it "euclidian should be a number < 1 and > 0" do
      @euclidian = GeneralProfile.euclidian_distance(1, 2)
      
      @euclidian.should be < 1
      @euclidian.should be > 0 
    end
    
    it "should create new comparison record" do
      expect { GeneralProfile.euclidian_distance(1, 2) }.to change(Comparison, :count).by(1)
    end
  end
end
