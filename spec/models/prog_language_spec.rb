require 'spec_helper'

describe ProgLanguage do
  before do
    @prog_language = ProgLanguage.create(language: "Java")
  end
  
  subject { @prog_language }
  
  it { should respond_to(:language)}
  
  it { should respond_to(:assignments) }
  
  it { should be_valid }
end