require 'spec_helper'

describe Subject do

  let(:teacher) { FactoryGirl.create(:teacher) }
  before do
    @subject = teacher.subjects.build(title: "Java programming")
  end
  
  subject { @subject }

  it { should respond_to(:title) }
  
  it { should respond_to(:assignments) }
  its(:teacher) { should == teacher }
  
  it { should be_valid }
  
  describe "when teacher_id is not present" do
    before { @subject.teacher_id = nil }
    it { should_not be_valid }
  end
end