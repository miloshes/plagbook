require 'spec_helper'

describe SchoolYear do
  
  let(:user) { FactoryGirl.create(:user) }
  before do
    @school_year = user.school_years.build(year: 2012)
  end
  
  subject { @school_year }
  
  it { should respond_to(:year) }
  
  its(:user) { should == user }
end