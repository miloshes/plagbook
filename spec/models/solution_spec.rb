require 'spec_helper'

describe Solution do

  let(:user) { FactoryGirl.create(:user) }
  before do
    @solution = user.solutions.build(filename: "file.java", assignment_id: 1)
  end

  subject { @solution }

  it { should respond_to(:filename) }
  it { should respond_to(:source_code) }
  it { should respond_to(:user_id) }
  it { should respond_to(:assignment_id) }
  it { should respond_to(:user) }
  it { should respond_to(:general_profile) }
  its(:user) { should == user }
  
  it { should be_valid }

  describe "when user_id is not present" do
    before { @solution.user_id = nil }
    it { should_not be_valid }
  end
  
  describe "when assignment_id is not present" do
    before { @solution.assignment = nil }
    it { should_not be_valid }
  end
  
  describe "when filename is not present" do
    before { @solution.filename = " " }
    it { should_not be_valid }
  end
  
  describe "compute general metrics" do
    before {@solution.source_code =
  "class Operations{
  //potrebujem dva zasobniky, B1 splna pomocnu ulohu
  Stack zasoba = new Stack(); //zasobnik1
  Stack B1 = new Stack();//zasobnik2 pomocny
  public Operations(String FileName){
    //v konstruktore nacitam do zasobnika zasoba subor
    Load(FileName);
  }
  
  //nacitanie zo suboru do zasobnika
  private void Load(String FileName){
    char B;
    //subor ako vstup musi byt otvoreny standardnymi metodami
    InputFile IF = new InputFile(FileName);
    //cyklus prejde po vsetkych znakoch v subore
    while (true){
      //do B sa nacitavaju znaky zo suboru
      B = IF.readBracket();
      //ak readbracket nacita '0' znamena to ze je koniec suboru atreba skoncit
      if (B=='0') {
        break;
      }else{  
        //System.out.print(B);
        //ak je znak nacitany tak ho vlozim do zasobnika
        zasoba.PUSH(new Item(B));
      }
    }
    System.out.println(\" je nacitane v zasobniku...\");
    //zasoba.RPRINT();
    //nakoniec musim zavriet subor
    IF.closeFile();
  
  }
  }"
      }
    it "should create new profile" do
      expect { @solution.compute_general_metrics }.to change(GeneralProfile, :count).by(1)
    end
  end
  
  describe "suspicious comparison" do
    before do
      @solution1 = FactoryGirl.create(:solution, assignment_id: 1)
      @gen_prof1 = FactoryGirl.create(:general_profile, user_id: @solution1.user_id)
      @solution2 = FactoryGirl.create(:solution, assignment_id: 1)
      @gen_prof2 = FactoryGirl.create(:general_profile, user_id: @solution2.user_id)
    end
    
    it "shoudl create new comparison" do
      expect { @solution.compare_suspicious }.to change(Comparison, :count).by(1)
    end
  end
  
end