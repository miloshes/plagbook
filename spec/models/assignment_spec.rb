require 'spec_helper'

describe Assignment do

  let(:subject1) { FactoryGirl.create(:subject) }
  
	before do
	   @assignment = subject1.assignments.build(name: "Basics", tags: "variable, if", description: "for noobs")
	end
	subject { @assignment }
	
	it { should respond_to(:name) }
	it { should respond_to(:tags) }
	it { should respond_to(:description) }
	
	it { should respond_to(:solutions) }
	it { should respond_to(:prog_languages)}
	its(:subject) { should == subject1 }
	
	it { should be_valid }
	
	describe "when name is not present" do
    before { @assignment.name = " " }
    it { should_not be_valid }
  end
end