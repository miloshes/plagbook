namespace :db do
  desc "Fill database with sample data"
  task :populate => :environment do
    Rake::Task['db:reset'].invoke
		make_groups
	  make_users
		make_subjects
		make_assignments
    make_solutions
    #make_prog_languages
    #make_school_years
  end
end

def make_groups
  Group.create!(:name => "root")
	Group.create!(:name => "teacher")
	Group.create!(:name => "student")
end

def make_users
	User.create!(:first_name => "Milos",
				 :last_name => "Blasko",
				 :login => "root",
         :password => "root123",
         :password_confirmation => "root123",
         :group_id => 1)
  
	User.create!(:first_name => "teacher",
				 :last_name => "User",
				 :login => "teacher",
         :password => "foobar",
         :password_confirmation => "foobar",
         :group_id => 2)
  
	User.create!(:first_name => "student",
				 :last_name => "User",
				 :login => "student",
         :password => "foobar",
         :password_confirmation => "foobar",
         :group_id => 3)
  
  99.times do |n|
    first_name  = Faker::Name.first_name
	  last_name = Faker::Name.last_name
	  login = "example-#{n+1}@railstutorial.org"
    password  = "password"
    User.create!(:first_name => first_name,
				   :last_name => last_name,
				   :login => login,
           :password => password,
           :password_confirmation => password,
           :group_id => 3)
  end
end

def make_subjects
	  Subject.create!(:title => "OOP", :teacher_id => User.find_by_login("teacher").id)
end

def make_assignments
    subject = Subject.first
    subject.assignments.create!(:name => "Basics", :description => "Just basic stuff", :tags => "java, basic, class, inheritance")
end

def make_solutions
    users = User.all(limit: 6)
    50.times do
      filepath = Faker::Lorem.words(3)
      users.each { |user| user.solutions.create!(filepath: filepath, assignment_id: 1) }
    end
end

def make_prog_languages
  ProgLanguage.create!(:language => "java")
  ProgLanguage.create!(:language => "c")
end

def make_school_year
  User.all.each do |user|
    if(user.group_id == Group.find_by_name("student").id)
      user.schoolyears.create!(:year => [2010, 2011].sample)
    end
  end
end