# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Plagbook::Application.initialize!

#application constants
STATUS_BASE = "base"
STATUS_ADDED = "added"
STATUS_WEIGHT = "weight"
STATUS_SUSPICIOUS = "suspicious"

LANG_JAVA = 1
LANG_C = 2

PROFILE_GENERAL = "general"
PROFILE_JAVA = "java"
PROFILE_C = "c"
PROFILE_JOINED = "joined-"

#application variables

#COMPARISON_METHOD = "distance_measure"
COMPARISON_METHOD = "euclidian_distance"
#COMPARISON_METHOD = "cosine_distance"

#PROFILE_ADJUSTMENT_METHOD = "average_2"
#PROFILE_ADJUSTMENT_METHOD = "average_all"
PROFILE_ADJUSTMENT_METHOD = "median"

#SUSPICIOUS_TRESHOLD = 0
COMPARE_PROFILES = true   # compare user profiles vs. solution profiles
JOIN_PROFILES = true      # when comparing, join general and language profiles
WEIGHTENING = true       # compute and use attribute weights when comparing
ADJUSTMENT_TRESHOLD = 0.1 # only adjust when the difference is less than treshold
NUMBER_OF_BASE_SOLUTIONS = 5
